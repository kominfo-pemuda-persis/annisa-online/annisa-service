-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for osx10.16 (x86_64)
--
-- Host: localhost    Database: annisa
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_login`
--

DROP TABLE IF EXISTS `t_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_login` (
  `id_login` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `npa` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activ_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_confirm` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_login`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `t_login_npa_foreign` (`npa`),
  CONSTRAINT `t_login_npa_foreign` FOREIGN KEY (`npa`) REFERENCES `t_anggota` (`npa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_login`
--

LOCK TABLES `t_login` WRITE;
/*!40000 ALTER TABLE `t_login` DISABLE KEYS */;
INSERT INTO `t_login` VALUES ('07920d2fddac48248dbe135cb0003d4a','99.0010','99.0010','anggota1@gmail.com','$2y$10$yjamvaGWXckM7qSLOIZUH.J5u9VX.ucKnivI.RN8SpqouWK/Jc00.','',1,'','',1,'2021-06-21 13:18:46','2021-06-21 13:18:46'),('169f2107f5074d658882f7363a4115c7','99.0007','99.0007','anggota2@gmail.com','$2y$10$d2n6MPSg5bwIk.S22kgM4uTa8jyGJzfrkp/D8VxM5O98z5fFAD9zW','',1,'','',1,'2021-06-21 13:18:45','2021-06-21 13:18:45'),('1f20756648fa423cb34205b5cb1ecbe9','99.0011','99.0011','anggota3@gmail.com','$2y$10$41N67qDLmQmvLrpPHUjp1.xPZir9XOiWDxL7HJ6nzO8hIOWqw6St2','',1,'','',1,'2021-06-21 13:18:46','2021-06-21 13:18:46'),('2b54c84230884b1fabc3dcf22ef9aae6','99.0008','99.0008','anggota4@gmail.com','$2y$10$.OPMMji5x.sZcM1d4hKZTeIXP8.ctYyoflVtkIaXPZjFf6aQYLkyq','',1,'','',1,'2021-06-21 13:18:45','2021-06-21 13:18:45'),('4325d8dae15541649eff795273bc78b3','99.0004','99.0004','anggota5@gmail.com','$2y$10$vhto.G5OmaoyYwzKcnNoRe64isfBDnWFIMinvVFxq9HYnhQpLcUKW','',1,'','',1,'2021-06-21 13:18:44','2021-06-21 13:18:44'),('4b178aff032f4c8081c45541492cc354','99.0001','99.0001','anggota6@gmail.com','$2y$10$mcE4Pl0Ck/d6SWifhJ3MIOyrfDXeUJJkcVP6sbo6w4aAChFPRuLS.','',1,'','',1,'2021-06-21 13:18:43','2021-06-21 13:18:43'),('a36116adc314477087957b6831f0ee52','99.0005','99.0005','anggota7@gmail.com','$2y$10$apwpoiGZKI5QyRKOv5X5T.BqWPDo.9gFrkLUIBWj3FE/WQve9cL/q','',1,'','',1,'2021-06-21 13:18:44','2021-06-21 13:18:44'),('b6c442b34d3342b5b4146b8a3409fe2c','99.0006','99.0006','anggota8@gmail.com','$2y$10$Lks7gRceXWz/O7.8zai9x.duXeYyJrE3OFLxgu.kJfzjaqZQssu7K','',1,'','',1,'2021-06-21 13:18:45','2021-06-21 13:18:45'),('beee3a9caf374c2aa37623428f95513d','99.0002','99.0002','anggota9@gmail.com','$2y$10$PD11kTvs4KojKtKS5Hx0f.Gg2TwPAMrn/QBcCh92DiozPR6sxW/WW','',1,'','',1,'2021-06-21 13:18:44','2021-06-21 13:18:44'),('c516cb52e3b14c3f9e8b2a3bb5d992a8','99.0009','99.0009','anggota10@gmail.com','$2y$10$/QdP.j/GSv9SoJTcjkJSjuczW/l5ExUdbIeTdEhX1yk34GjI6Xsa.','',1,'','',1,'2021-06-21 13:18:45','2021-06-21 13:18:45');
/*!40000 ALTER TABLE `t_login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-06  8:53:35
