CREATE TABLE t_kejamiyyahan_pc
(
    id                      int unsigned auto_increment
        PRIMARY KEY,
    kd_pc                   varchar(10)  NOT NULL,
    kd_pd                   varchar(10)  NOT NULL,
    kd_pw                   varchar(10)  NOT NULL,
    kd_monografi_pc         bigint unsigned NOT NULL,
    provinsi                varchar(2)   NOT NULL,
    kota                    varchar(4)   NOT NULL,
    kecamatan               varchar(7)   NOT NULL,
    ketua                   varchar(191) NOT NULL,
    wkl_ketua               varchar(191) NULL,
    sekretaris              varchar(191) NOT NULL,
    wkl_sekretaris          varchar(191) NULL,
    bendahara               varchar(191) NOT NULL,
    wkl_bendahara           varchar(191) NULL,
    bid_jamiyyah            varchar(191) NULL,
    wkl_bid_jamiyyah        varchar(191) NULL,
    bid_kaderisasi          varchar(191) NULL,
    wkl_bid_kaderisasi      varchar(191) NULL,
    bid_administrasi        varchar(191) NULL,
    wkl_bid_administrasi    varchar(191) NULL,
    bid_pendidikan          varchar(191) NULL,
    wkl_bid_pendidikan      varchar(191) NULL,
    bid_dakwah              varchar(191) NULL,
    wkl_bid_dakwah          varchar(191) NULL,
    bid_humas_publikasi     varchar(191) NULL,
    wkl_bid_humas_publikasi varchar(191) NULL,
    bid_hal                 varchar(191) NULL,
    wkl_bid_hal             varchar(191) NULL,
    bid_or_seni             varchar(191) NULL,
    wkl_bid_or_seni         varchar(191) NULL,
    bid_sosial              varchar(191) NULL,
    wkl_bid_sosial          varchar(191) NULL,
    bid_ekonomi             varchar(191) NULL,
    wkl_bid_ekonomi         varchar(191) NULL,
    penasehat1              varchar(191) NULL,
    penasehat2              varchar(191) NULL,
    penasehat3              varchar(191) NULL,
    penasehat4              varchar(191) NULL,
    pembantu_umum1          varchar(191) NULL,
    pembantu_umum2          varchar(191) NULL,
    pembantu_umum3          varchar(191) NULL,
    hari                    varchar(191) NOT NULL,
    pukul                   time         NOT NULL,
    musycab_terakhir_m      date         NOT NULL,
    musycab_terakhir_h      date         NOT NULL,
    anggota_biasa           int NULL,
    anggota_tersiar         int NULL,
    anggota_istimewa        int NULL,
    tdk_her                 int NULL,
    mutasi_ke_persis        int NULL,
    mutasi_tempat           int NULL,
    mengundurkan_diri       int NULL,
    meninggal_dunia         int NULL,
    biasa                   int NULL,
    istimewa                int NULL,
    tersiar                 int NULL,
    created_at              timestamp NULL,
    updated_at              timestamp NULL,
    deleted_at              timestamp NULL,
    CONSTRAINT t_kejamiyyahan_pc_kd_monografi_pc_foreign
        FOREIGN KEY (kd_monografi_pc) REFERENCES t_monografi_pc (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pc_kd_pc_foreign
        FOREIGN KEY (kd_pc) REFERENCES t_pc (kd_pc)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pc_kd_pd_foreign
        FOREIGN KEY (kd_pd) REFERENCES t_pd (kd_pd)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pc_kd_pw_foreign
        FOREIGN KEY (kd_pw) REFERENCES t_pw (kd_pw)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pc_kecamatan_foreign
        FOREIGN KEY (kecamatan) REFERENCES t_kecamatan (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pc_kota_foreign
        FOREIGN KEY (kota) REFERENCES t_kabupaten (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pc_provinsi_foreign
        FOREIGN KEY (provinsi) REFERENCES t_provinsi (id)
            ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;

