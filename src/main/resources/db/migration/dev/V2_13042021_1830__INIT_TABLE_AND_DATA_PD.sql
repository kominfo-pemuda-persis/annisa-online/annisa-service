CREATE TABLE t_pd
(
    kd_pd      varchar(10)                         NOT NULL
        PRIMARY KEY,
    kd_pw      varchar(10)                         NOT NULL,
    nama_pd    varchar(191)                        NOT NULL,
    diresmikan date                                NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD001', 'PW001', 'Kabupaten Bandung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD002', 'PW001', 'Kota Bandung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD003', 'PW001', 'Kabupaten Tasikmalaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD004', 'PW001', 'Kabupaten Sukabumi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD005', 'PW001', 'Kota Cimahi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD006', 'PW001', 'Kabupaten Bima', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD007', 'PW001', 'Kabupaten Garut', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD008', 'PW001', 'Kabupaten Cianjur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD009', 'PW001', 'Kabupaten Sumedang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD010', 'PW001', 'Kota Tasikmalaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD011', 'PW004', 'Kabupaten Gorontalo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD012', 'PW001', 'Kabupaten Karawang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD013', 'PW008', 'Kabupaten Asahan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD015', 'PW002', 'Kabupaten Pasuruan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD016', 'PW001', 'Kabupaten Bekasi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD017', 'PW001', 'Kabupaten Ciamis', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD018', 'PW001', 'Kabupaten Bandung Barat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD019', 'PW001', 'Kabupaten Cirebon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD020', 'PW001', 'Kabupaten Subang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD021', 'PW001', 'Kabupaten Majalengka', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD022', 'PW003', 'Jakarta Timur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD023', 'PW003', 'Jakarta Barat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD024', 'PW003', 'Jakarta Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD025', 'PW003', 'Jakarta Pusat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD026', 'PW011', 'Kabupaten Batubara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD027', 'PW001', 'Kota Banjar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD028', 'PW008', 'Kota Medan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD029', 'PW008', 'Kabupaten Deli Serdang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PD030', 'PW001', 'Kota Bekasi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pd (kd_pd, kd_pw, nama_pd, diresmikan, created_at, updated_at)
VALUES ('PDXYZ', 'PW001', 'Belum Ada PD', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

