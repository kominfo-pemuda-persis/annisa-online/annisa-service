CREATE TABLE t_anggota_organisasi
(
    id_organisasi   int auto_increment
        PRIMARY KEY,
    id_anggota      varchar(32)  NOT NULL,
    nama_organisasi varchar(100) NOT NULL,
    jabatan         varchar(50) NULL,
    tingkat         varchar(20) NULL,
    tahun_mulai     int NULL,
    tahun_selesai   int NULL,
    deleted_at      timestamp NULL,
    lokasi          varchar(191) NOT NULL
) COLLATE=utf8mb4_unicode_ci;

