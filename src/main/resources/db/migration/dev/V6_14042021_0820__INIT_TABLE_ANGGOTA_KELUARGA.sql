CREATE TABLE t_anggota_keluarga
(
    id_keluarga    int auto_increment
        PRIMARY KEY,
    id_anggota     varchar(32) NOT NULL,
    nama_keluarga  varchar(70) NOT NULL,
    hubungan       enum('AYAH', 'IBU', 'SUAMI', 'ANAK') NOT NULL,
    jumlah_anak    int NULL,
    alamat         VARCHAR(255) NULL,
    id_otonom      int NULL,
    deleted_at     timestamp NULL,
    keterangan     varchar(191) NULL,
    status_anggota enum('Anggota', 'Bukan Anggota') NOT NULL
) COLLATE=utf8mb4_unicode_ci;

