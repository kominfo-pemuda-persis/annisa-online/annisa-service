-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for osx10.16 (x86_64)
--
-- Host: localhost    Database: annisa
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_anggota`
--

DROP TABLE IF EXISTS `t_anggota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_anggota` (
  `id_anggota` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `npa` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status_merital` enum('SINGLE','MENIKAH','JANDA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pw` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pd` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pc` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pj` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_pj` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kecamatan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desa` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gol_darah` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telpon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telpon2` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kk` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ktp_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_keanggotaan` enum('BIASA','TERSIAR') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_aktif` enum('ACTIVE','NON-ACTIVE','TIDAK HEREGISTRASI','MUTASI KE PERSISTRI','MENINGGAL','MENGUNDURKAN DIRI') COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_otonom` smallint(6) NOT NULL,
  `masa_aktif_kta` date DEFAULT NULL,
  `reg_date` datetime NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `pendidikan_terakhir` int(11) NOT NULL DEFAULT 11,
  `level_pkp` smallint(6) NOT NULL DEFAULT 0,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_anggota`),
  KEY `npa` (`npa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_anggota`
--

LOCK TABLES `t_anggota` WRITE;
/*!40000 ALTER TABLE `t_anggota` DISABLE KEYS */;
INSERT INTO `t_anggota` VALUES ('002a8dae1f8d494fb03b23786d0ba6c1','99.0010','Asmadi Adika Sinaga','Bekasi','1990-02-27','MENIKAH','Guru','PW-1','PD-XYZ','PC.122','PJ.10','Samarinda','32','3273','3273050','1101031009','O','pwahyuni@wacana.com','80989999','81000000','','','','Jln. Setia Budi No. 731, Palu 61869, SulTeng','BIASA','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,11,0,'Machine'),('00628e6ec8f4410e83a04c43032464c6','99.0007','Cakrajiya Putra','Palembang','1993-01-06','SINGLE','Guru','PW-1','PD-7','PC.67','PJ.12','Sibolga','31','3173','3173040','1101031009','AB','kasiyah.suryono@gmail.co.id','80989999','81000000','','','','Kpg. Bata Putih No. 51, Serang 91755, Jambi','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,9,0,'Machine'),('008f9a9d512c40fd994e8b19fc26315a','99.0011','Praba Pradipta M.Pd','Medan','1994-11-24','MENIKAH','Guru','PW-XYZ','PD-30','PC.165','PJ.13','Blitar','32','3273','3273050','1101031009','B','yunita.gunawan@hartati.sch.id','80989999','81000000','','','','Ds. Bakaru No. 928, Banda Aceh 52058, Gorontalo','BIASA','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,12,0,'Machine'),('00ad799eb7a240debd933fc7cf1f864e','99.0008','Kawaca Ismail Ardianto','Dumai','1966-01-19','SINGLE','Guru','PW-1','PD-2','PC.22','PJ.4','Madiun','31','3174','3174040','1101031009','O','maryanto.pranowo@prastuti.tv','80989999','81000000','','','','Ki. Teuku Umar No. 179, Salatiga 13551, SulSel','BIASA','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,13,0,'Machine'),('014bb84a08f34004ad8261b13c28a230','99.0004','Kardi Rajasa','Pasuruan','1995-01-04','SINGLE','Guru','PW-1','PD-2','PC.102','PJ.11','Pangkal Pinang','31','3174','3174070','1101031009','O','cmandasari@natsir.web.id','80989999','81000000','','','','Gg. Kebonjati No. 829, Ambon 62391, Bali','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,13,0,'Machine'),('01524a48f65b471699cfa36b4806f178','99.0001','Kenes Wibisono','Mataram','1935-10-22','SINGLE','Guru','PW-1','PD-2','PC.32','PJ.2','Batam','32','3273','3273020','1101031009','AB','wisnu.najmudin@firmansyah.my.id','80989999','81000000','','','','Dk. Gremet No. 945, Tebing Tinggi 74937, SulTeng','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,3,0,'Machine'),('016216947ded4c5cb682b4874aaf5f57','99.0005','Leo Hamzah Najmudin','Palopo','2000-03-05','MENIKAH','Guru','PW-1','PD-XYZ','PC.164','PJ.13','Banjarbaru','32','3273','3273180','1101031009','AB','olivia.pudjiastuti@yahoo.com','80989999','81000000','','','','Gg. Yosodipuro No. 305, Dumai 94155, Banten','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,17,0,'Machine'),('016990ba22f849e1907139cd8982d89c','99.0006','Gatra Widodo','Padangsidempuan','1949-10-06','SINGLE','Guru','PW-1','PD-9','PC.47','PJ.6','Sukabumi','31','3174','3174040','1101031009','O','emil61@salahudin.desa.id','80989999','81000000','','','','Jr. Bakau No. 418, Batam 37101, Maluku','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,1,0,'Machine'),('019a3fc59c8b488582810618fe19cb5f','99.0002','Erik Kuswoyo M.Ak','Balikpapan','1921-10-26','SINGLE','Guru','PW-1','PD-8','PC.97','PJ.1','Blitar','32','3273','3273030','1101031009','O','kpratiwi@yahoo.co.id','80989999','81000000','','','','Kpg. Sukabumi No. 562, Batu 76470, JaTim','BIASA','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,11,0,'Machine'),('01b5f37a6ddb4f44befb5cda0f1741af','99.0009','Viman Paiman Mahendra','Kupang','2013-01-05','SINGLE','Guru','PW-1','PD-26','PC.107','PJ.5','Batu','31','3173','3173030','1101031009','AB','hutapea.raden@suwarno.biz.id','80989999','81000000','','','','Jr. Hang No. 384, Banjarmasin 99731, KalTeng','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,15,0,'Machine'),('01ea8c52ff2441229150cde9b997feac','99.0003','Darsirah Raden Siregar S.Psi','Ambon','1937-01-03','SINGLE','Guru','PW-1','PD-6','PC.70','PJ.11','Administrasi Jakarta Selatan','32','3273','3273160','1101031009','B','xaryani@gmail.co.id','80989999','81000000','','','','Gg. Rumah Sakit No. 152, Padang 99303, KalTeng','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,10,0,'Machine'),('023c43a69ad9465e9301985df298a771','79.9322','Cahyo Samosir','Tidore Kepulauan','1974-11-11','SINGLE','Guru','PW-1','PD-20','PC.145','PJ.6','Samarinda','31','3173','3173040','1101031009','B','kasim18@gunawan.com','80989999','81000000','','','','Jr. Gotong Royong No. 473, Gorontalo 15247, JaTeng','BIASA','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,2,0,'Machine'),('0258c967d62145a6b2bfe8ee6f41920f','54.9166','Eja Luhung Budiyanto S.Pt','Payakumbuh','2017-08-21','MENIKAH','Guru','PW-1','PD-1','PC.115','PJ.6','Bekasi','32','3273','3273020','1101031009','A','anggabaya94@mustofa.name','80989999','81000000','','','','Jr. B.Agam Dlm No. 680, Manado 24045, NTT','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,11,0,'Machine'),('02b1f630183a496ba80092ff5f36b6d0','70.4308','Empluk Pratama','Pariaman','2010-11-05','MENIKAH','Guru','PW-1','PD-2','PC.30','PJ.14','Tual','31','3174','3174070','1101031009','A','wpangestu@yahoo.com','80989999','81000000','','','','Ki. Laswi No. 847, Cimahi 17310, BaBel','TERSIAR','ACTIVE','default.png',3,'2021-06-21','2021-06-21 20:18:38','2021-06-22 11:34:43','Machine','2021-06-22 04:34:43',NULL,15,0,'Machine');
/*!40000 ALTER TABLE `t_anggota` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-06  8:51:56
