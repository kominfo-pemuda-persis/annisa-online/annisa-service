CREATE TABLE t_anggota_pkp
(
    id              bigint auto_increment
        PRIMARY KEY,
    id_anggota      varchar(32)  NOT NULL,
    tanggal_masuk   date         NOT NULL,
    tanggal_selesai date         NOT NULL,
    lokasi          varchar(100) NOT NULL,
    deleted_at      timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

