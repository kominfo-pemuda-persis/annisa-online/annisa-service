CREATE TABLE t_anggota
(
    id_anggota          varchar(32)                         NOT NULL
        PRIMARY KEY,
    npa                 varchar(10) NULL,
    nama_lengkap        varchar(50)                         NOT NULL,
    tempat_lahir        varchar(50)                         NOT NULL,
    tanggal_lahir       date                                NOT NULL,
    status_merital      enum('SINGLE', 'MENIKAH', 'JANDA') NOT NULL,
    pekerjaan           varchar(25)                         NOT NULL,
    pw                  varchar(10) NULL,
    pd                  varchar(10) NULL,
    pc                  varchar(10) NULL,
    pj                  varchar(10) NULL,
    nama_pj             varchar(100) NULL,
    provinsi            varchar(10)                         NOT NULL,
    kota                varchar(10)                         NOT NULL,
    kecamatan           varchar(10)                         NOT NULL,
    desa                varchar(100)                        NOT NULL,
    gol_darah           varchar(3)                          NOT NULL,
    email               varchar(50)                         NOT NULL,
    no_telpon           varchar(15)                         NOT NULL,
    no_telpon2          varchar(15)                         NOT NULL,
    nik                 varchar(20) NULL,
    kk                  varchar(20) NULL,
    ktp_image           varchar(100) NULL,
    alamat              varchar(100)                        NOT NULL,
    jenis_keanggotaan   enum('BIASA', 'TERSIAR') NOT NULL,
    status_aktif        enum('ACTIVE', 'NON-ACTIVE', 'TIDAK HEREGISTRASI', 'MUTASI KE PERSISTRI', 'MENINGGAL', 'MENGUNDURKAN DIRI') NOT NULL,
    foto                varchar(100)                        NOT NULL,
    id_otonom           smallint                            NOT NULL,
    masa_aktif_kta      date NULL,
    reg_date            datetime                            NOT NULL,
    last_updated        datetime                            NOT NULL,
    created_by          varchar(50)                         NOT NULL,
    created_date        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at          timestamp NULL,
    pendidikan_terakhir int       DEFAULT 11                NOT NULL,
    level_pkp           SMALLINT                            NOT NULL,
    updated_by          varchar(50)                         NOT NULL
--     constraint t_anggota_desa_foreign
--         foreign key (desa) references t_desa (id)
--             on delete cascade,
--     constraint t_anggota_kecamatan_foreign
--         foreign key (kecamatan) references t_kecamatan (id)
--             on delete cascade,
--     constraint t_anggota_kota_foreign
--         foreign key (kota) references t_kabupaten (id)
--             on delete cascade,
--     constraint t_anggota_pc_foreign
--         foreign key (pc) references t_pc (kd_pc)
--             on delete cascade,
--     constraint t_anggota_pd_foreign
--         foreign key (pd) references t_pd (kd_pd)
--             on delete cascade,
--     constraint t_anggota_pendidikan_terakhir_foreign
--         foreign key (pendidikan_terakhir) references t_master_pendidikan (id_tingkat_pendidikan)
--             on delete cascade,
--     constraint t_anggota_provinsi_foreign
--         foreign key (provinsi) references t_provinsi (id)
--             on delete cascade,
--     constraint t_anggota_pw_foreign
--         foreign key (pw) references t_pw (kd_pw)
--             on delete cascade
) COLLATE=utf8mb4_unicode_ci;

CREATE
index npa
	ON t_anggota (npa);

