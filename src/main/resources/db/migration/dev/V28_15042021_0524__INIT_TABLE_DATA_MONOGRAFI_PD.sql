CREATE TABLE t_monografi_pd
(
    id                           bigint unsigned auto_increment
        PRIMARY KEY,
    kd_pd                        varchar(10)  NOT NULL,
    nama_pd                      varchar(50)  NOT NULL,
    kd_pw                        varchar(10)  NOT NULL,
    provinsi                     varchar(2)   NOT NULL,
    kota                         varchar(4)   NOT NULL,
    latitude                     double       NOT NULL,
    longitude                    double       NOT NULL,
    alamat_utama                 varchar(191) NOT NULL,
    alamat_alternatif            varchar(191) NOT NULL,
    no_kontak                    varchar(15)  NOT NULL,
    email                        varchar(191) NULL,
    luas                         int          NOT NULL,
    bw_utara                     varchar(75)  NOT NULL,
    bw_selatan                   varchar(75)  NOT NULL,
    bw_timur                     varchar(75)  NOT NULL,
    bw_barat                     varchar(75)  NOT NULL,
    jarak_dari_ibukota_negara    DOUBLE       NOT NULL,
    jarak_dari_ibukota_provinsi  DOUBLE       NOT NULL,
    jarak_dari_ibukota_kabupaten DOUBLE       NOT NULL,
    foto                         varchar(50) NULL,
    created_at                   timestamp NULL,
    updated_at                   timestamp NULL,
    deleted_at                   timestamp NULL,
    created_by                   varchar(50)  NOT NULL,
    updated_by                   varchar(50)  NOT NULL,
    CONSTRAINT t_monografi_pd_email_unique
        UNIQUE (email),
    CONSTRAINT t_monografi_pd_kd_pw_foreign
        FOREIGN KEY (kd_pw) REFERENCES t_pw (kd_pw)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pd_kota_foreign
        FOREIGN KEY (kota) REFERENCES t_kabupaten (id)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pd_provinsi_foreign
        FOREIGN KEY (provinsi) REFERENCES t_provinsi (id)
            ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;

