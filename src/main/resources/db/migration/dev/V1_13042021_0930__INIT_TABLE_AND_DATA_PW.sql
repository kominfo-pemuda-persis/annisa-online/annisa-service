CREATE TABLE t_pw
(
    kd_pw      varchar(10)                         NOT NULL
        PRIMARY KEY,
    nama_pw    varchar(191)                        NOT NULL,
    diresmikan date                                NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW001', 'Jawa Barat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW002', 'Jawa Timur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW003', 'DKI Jakarta', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW004', 'Gorontalo', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW005', 'Banten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW006', 'Bali', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW007', 'Yogyakarta', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW008', 'Sumatera Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW009', 'Lampung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW010', 'Maluku', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pw (kd_pw, nama_pw, diresmikan, created_at, updated_at)
VALUES ('PW011', 'Sulawesi Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);