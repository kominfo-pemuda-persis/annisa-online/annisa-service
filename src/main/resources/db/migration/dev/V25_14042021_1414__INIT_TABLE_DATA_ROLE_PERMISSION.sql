CREATE TABLE t_role
(
    id_role      int unsigned auto_increment
        PRIMARY KEY,
    name         varchar(191) NOT NULL,
    display_name varchar(191) NOT NULL,
    description  varchar(191) NULL,
    status_aktif tinyint(1) DEFAULT 1 NOT NULL
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (1, 'anggota', 'Anggota', 'Memiliki NPA', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (2, 'superadmin', 'Super Admin', 'Memiliki NPA, Tim Pengembang Sistem yang diberi SK Oleh Ketua PP', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (3, 'admin_pp', 'Admin PP', 'Tasykil Kominfo PP atau yang mendapatkan SK dari Ketua PP', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (4, 'tasykil_pp', 'Tasykil PP', 'Pimhar PP, Tasykil PP, Tasykil Lembaga Khusus PP', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (5, 'admin_pw', 'Admin PW', 'Tasykil Kominfo PW atau yang mendapatkan SK dari Ketua PW', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (6, 'tasykil_pw', 'Tasykil PW', 'Pimhar PW, Tasykil PW, Tasykil Lembaga Khusus PW', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (7, 'admin_pd', 'Admin PD', 'Tasykil Kominfo PD atau yang mendapatkan SK dari Ketua PD', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (8, 'tasykil_pd', 'Tasykil PD', 'Pimhar PD, Tasykil PD, Tasykil Lembaga Khusus PD', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (9, 'admin_pc', 'Admin PC', 'Admin PC atau yang mendapatkan SK dari Ketua PC', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (10, 'tasykil_pc', 'Tasykil PC', 'Tasykil Kominfo PC atau yang mendapatkan SK dari Ketua PC', 1);
INSERT INTO t_role (id_role, name, display_name, description, status_aktif)
VALUES (11, 'admin_pj', 'Admin PJ', 'Admin Kominfo PJ atau yang mendapatkan SK dari Ketua PJ', 1);


CREATE TABLE t_role_user
(
    role_id int unsigned NOT NULL,
    user_id varchar(32) NOT NULL,
    PRIMARY KEY (user_id, role_id),
    CONSTRAINT t_role_user_role_id_foreign
        FOREIGN KEY (role_id) REFERENCES t_role (id_role)
            ON UPDATE CASCADE ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;



CREATE TABLE t_permission_user
(
    user_id    int unsigned NOT NULL,
    module     varchar(191) NOT NULL,
    permission varchar(191) NOT NULL,
    PRIMARY KEY (user_id, module, permission)
) COLLATE=utf8mb4_unicode_ci;


CREATE TABLE t_permission_role
(
    role_id    int unsigned NOT NULL,
    module     varchar(191) NOT NULL,
    permission varchar(191) NOT NULL,
    PRIMARY KEY (role_id, module, permission),
    CONSTRAINT t_permission_role_role_id_foreign
        FOREIGN KEY (role_id) REFERENCES t_role (id_role)
            ON UPDATE CASCADE ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_permission_role (role_id, module, permission)
VALUES (1, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'read_all');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'read_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'read_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'update_all');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'update_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'anggota', 'update_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'indexperformapw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'monografipw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'mutasi', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'mutasi', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'permission', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'role', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'users', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'users', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (3, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'read_all');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'read_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'read_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'update_all');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'update_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'anggota', 'update_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'indexperformapw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'monografipw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (4, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'read_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'read_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'update_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'anggota', 'update_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'indexperformapw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'monografipw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (5, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'read_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'read_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'update_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'anggota', 'update_pw');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'indexperformapw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipw', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipw', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipw', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'monografipw', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (6, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'anggota', 'read_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'anggota', 'update_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (7, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'anggota', 'read_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'anggota', 'update_pd');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipd', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipd', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipd', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipd', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (8, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'anggota', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'mutasi', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'users', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'users', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (9, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'anggota', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'anggota', 'read_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'anggota', 'update_pc');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'jamiyyah', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'jamiyyah', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipc', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipc', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipc', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipc', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'mutasi', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'users', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'users', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (10, 'users', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'anggota', 'read_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'anggota', 'update_pj');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'dashboard', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'indexperformapj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'indexperformapj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'indexperformapj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'indexperformapj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'jamiyyah', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'jamiyyah', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'monografipj', 'create');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'monografipj', 'delete');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'monografipj', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'monografipj', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'mutasi', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'mutasi', 'update');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'otonom', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'permission', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'role', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'users', 'read');
INSERT INTO t_permission_role (role_id, module, permission)
VALUES (11, 'users', 'update');


