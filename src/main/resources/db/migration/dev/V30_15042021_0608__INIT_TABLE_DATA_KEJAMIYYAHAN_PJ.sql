CREATE TABLE t_kejamiyyahan_pj
(
    id                        int unsigned auto_increment
        PRIMARY KEY,
    kode_pj                   varchar(6)   NOT NULL,
    nama_pj                   varchar(50)  NOT NULL,
    kd_monografi_pj           bigint unsigned NOT NULL,
    kd_pc                     varchar(10)  NOT NULL,
    kd_pd                     varchar(10)  NOT NULL,
    kd_pw                     varchar(10)  NOT NULL,
    provinsi                  varchar(2)   NOT NULL,
    kabupaten                 varchar(4)   NOT NULL,
    kecamatan                 varchar(7)   NOT NULL,
    desa                      varchar(10)  NOT NULL,
    ketua                     varchar(191) NOT NULL,
    sekretaris                varchar(191) NOT NULL,
    bendahara                 varchar(191) NOT NULL,
    hari_ngantor              enum('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Ahad') NOT NULL,
    waktu_ngantor             time         NOT NULL,
    musjam_terakhir_masehi    datetime     NOT NULL,
    musjam_terakhir_hijriyyah datetime     NOT NULL,
    anggota_biasa             int          NOT NULL,
    anggota_luar_biasa        int          NOT NULL,
    tidak_herReg              varchar(191) NOT NULL,
    mutasi_persis             varchar(191) NOT NULL,
    mutasi_tempat             varchar(191) NOT NULL,
    meninggal_dunia           varchar(191) NOT NULL,
    mengundurkan_diri         varchar(191) NOT NULL,
    calon_anggota             varchar(191) NOT NULL,
    created_at                timestamp NULL,
    updated_at                timestamp NULL,
    deleted_at                timestamp NULL,
    CONSTRAINT t_kejamiyyahan_pj_desa_foreign
        FOREIGN KEY (desa) REFERENCES t_desa (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_kabupaten_foreign
        FOREIGN KEY (kabupaten) REFERENCES t_kabupaten (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_kd_monografi_pj_foreign
        FOREIGN KEY (kd_monografi_pj) REFERENCES t_monografi_pj (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_kd_pc_foreign
        FOREIGN KEY (kd_pc) REFERENCES t_pc (kd_pc)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_kd_pd_foreign
        FOREIGN KEY (kd_pd) REFERENCES t_pd (kd_pd)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_kd_pw_foreign
        FOREIGN KEY (kd_pw) REFERENCES t_pw (kd_pw)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_kecamatan_foreign
        FOREIGN KEY (kecamatan) REFERENCES t_kecamatan (id)
            ON DELETE CASCADE,
    CONSTRAINT t_kejamiyyahan_pj_provinsi_foreign
        FOREIGN KEY (provinsi) REFERENCES t_provinsi (id)
            ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;

