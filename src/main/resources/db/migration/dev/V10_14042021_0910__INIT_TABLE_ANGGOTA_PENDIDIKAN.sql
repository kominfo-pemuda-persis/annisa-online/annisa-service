CREATE TABLE t_anggota_pendidikan
(
    id_pendidikan        int auto_increment
        PRIMARY KEY,
    id_anggota           varchar(32)  NOT NULL,
    id_master_pendidikan int          NOT NULL,
    instansi             varchar(100) NOT NULL,
    jurusan              varchar(100) NULL,
    tahun_masuk          int          NOT NULL,
    tahun_keluar         int          NOT NULL,
    jenis_pendidikan     enum('FORMAL', 'NON FORMAL') DEFAULT 'Formal' NULL,
    deleted_at           timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

