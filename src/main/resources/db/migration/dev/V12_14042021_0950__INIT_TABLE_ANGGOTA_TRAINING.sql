CREATE TABLE t_anggota_training
(
    id_training     bigint auto_increment
        PRIMARY KEY,
    id_anggota      varchar(32) NOT NULL,
    nama_training   varchar(50) NULL,
    penyelenggara   varchar(50) NULL,
    tempat          varchar(255) NULL,
    tanggal_mulai   date NULL,
    tanggal_selesai date NULL,
    jenis           enum('TRAINING PEMUDA', 'TRAINING LUAR PEMUDA') NULL,
    deleted_at      timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

