CREATE TABLE t_monografi_pj
(
    id              bigint unsigned auto_increment
        PRIMARY KEY,
    kode_pj         varchar(6)   NOT NULL,
    nama_pj         varchar(50)  NOT NULL,
    kd_pc           varchar(10)  NOT NULL,
    kd_pd           varchar(10)  NOT NULL,
    kd_pw           varchar(10)  NOT NULL,
    provinsi        varchar(2)   NOT NULL,
    kabupaten       varchar(4)   NOT NULL,
    kecamatan       varchar(7)   NOT NULL,
    desa            varchar(10)  NOT NULL,
    latitude        double       NOT NULL,
    longitude       double       NOT NULL,
    email           varchar(50) NULL,
    no_telpon       varchar(15)  NOT NULL,
    alamat          varchar(100) NOT NULL,
    luas            varchar(191) NOT NULL,
    bw_timur        varchar(50)  NOT NULL,
    bw_barat        varchar(50)  NOT NULL,
    bw_selatan      varchar(50)  NOT NULL,
    bw_utara        varchar(50)  NOT NULL,
    jarak_provinsi  double       NOT NULL,
    jarak_kabupaten double       NOT NULL,
    photo           varchar(100) NULL,
    created_by      varchar(50)  NOT NULL,
    created_at      timestamp NULL,
    updated_at      timestamp NULL,
    deleted_at      timestamp NULL,
    updated_by      varchar(50)  NOT NULL,
    CONSTRAINT t_monografi_pj_email_unique
        UNIQUE (email),
    CONSTRAINT t_monografi_pj_desa_foreign
        FOREIGN KEY (desa) REFERENCES t_desa (id)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pj_kabupaten_foreign
        FOREIGN KEY (kabupaten) REFERENCES t_kabupaten (id)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pj_kd_pc_foreign
        FOREIGN KEY (kd_pc) REFERENCES t_pc (kd_pc)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pj_kd_pd_foreign
        FOREIGN KEY (kd_pd) REFERENCES t_pd (kd_pd)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pj_kd_pw_foreign
        FOREIGN KEY (kd_pw) REFERENCES t_pw (kd_pw)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pj_kecamatan_foreign
        FOREIGN KEY (kecamatan) REFERENCES t_kecamatan (id)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pj_provinsi_foreign
        FOREIGN KEY (provinsi) REFERENCES t_provinsi (id)
            ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;

