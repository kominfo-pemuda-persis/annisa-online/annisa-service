CREATE TABLE t_monografi_pc
(
    id                           bigint unsigned auto_increment
        PRIMARY KEY,
    kd_pc                        varchar(10)  NOT NULL,
    kd_pd                        varchar(10)  NOT NULL,
    kd_pw                        varchar(10)  NOT NULL,
    provinsi                     varchar(2)   NOT NULL,
    kota                         varchar(4)   NOT NULL,
    kecamatan                    varchar(7)   NOT NULL,
    latitude                     double       NOT NULL,
    longitude                    double       NOT NULL,
    alamat_utama                 varchar(191) NOT NULL,
    alamat_alternatif            varchar(191) NOT NULL,
    no_kontak                    varchar(15)  NOT NULL,
    email                        varchar(191) NULL,
    luas                         int          NOT NULL,
    bw_utara                     varchar(75)  NOT NULL,
    bw_selatan                   varchar(75)  NOT NULL,
    bw_timur                     varchar(75)  NOT NULL,
    bw_barat                     varchar(75)  NOT NULL,
    jarak_dari_ibukota_negara    double       NOT NULL,
    jarak_dari_ibukota_provinsi  double       NOT NULL,
    jarak_dari_ibukota_kabupaten double       NOT NULL,
    foto                         varchar(191) NULL,
    created_at                   timestamp NULL,
    updated_at                   timestamp NULL,
    deleted_at                   timestamp NULL,
    created_by                   varchar(50)  NOT NULL,
    updated_by                   varchar(50)  NOT NULL,
    CONSTRAINT t_monografi_pc_email_unique
        UNIQUE (email),
    CONSTRAINT t_monografi_pc_kd_pc_foreign
        FOREIGN KEY (kd_pc) REFERENCES t_pc (kd_pc)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pc_kd_pd_foreign
        FOREIGN KEY (kd_pd) REFERENCES t_pd (kd_pd)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pc_kd_pw_foreign
        FOREIGN KEY (kd_pw) REFERENCES t_pw (kd_pw)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pc_kecamatan_foreign
        FOREIGN KEY (kecamatan) REFERENCES t_kecamatan (id)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pc_kota_foreign
        FOREIGN KEY (kota) REFERENCES t_kabupaten (id)
            ON DELETE CASCADE,
    CONSTRAINT t_monografi_pc_provinsi_foreign
        FOREIGN KEY (provinsi) REFERENCES t_provinsi (id)
            ON DELETE CASCADE
) COLLATE=utf8mb4_unicode_ci;

