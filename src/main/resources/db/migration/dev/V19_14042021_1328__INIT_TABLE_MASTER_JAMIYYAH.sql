CREATE TABLE `t_master_jamiyyah`
(
    `id_jamiyyah`   int                                    NOT NULL AUTO_INCREMENT,
    `id_wilayah`    int                                    NOT NULL,
    `nama_pimpinan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
    `id_otonom`     int                                    NOT NULL,
    `pimpinan`      enum('PW','PD','PC','PC Non PD','Perwakilan PW') COLLATE utf8mb4_unicode_ci NOT NULL,
    `nomor`         int      DEFAULT NULL,
    `alamat`        text COLLATE utf8mb4_unicode_ci,
    `latitude`      double   DEFAULT NULL,
    `longitude`     double   DEFAULT NULL,
    `status_aktif`  smallint DEFAULT NULL,
    `deleted_at`    timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id_jamiyyah`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (1, 32, 'Jawa Barat  ', 3, 'PW', NULL, 'Gg. Muncang Kebon Kalapa Kota Bandung Jawa Barat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (2, 3273, 'Kota Bandung  ', 3, 'PD', NULL, 'Jl. Astana Anyar No. 310 Bandung 20442 Tlp. (022) 5202843', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (3, 3273180, 'Andir  ', 3, 'PC', NULL, 'Jl. Rajawali Timur No. 15/26 Bandung 40182', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (4, 3273050, 'Astana anyar  ', 3, 'PC', NULL, 'Jl. Asatana Anyar 310 Kelurahan Nyengseret Kota Bandung', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (5, 3273020, 'Babakan Ciparay  ', 3, 'PC', NULL, 'Jl. Pagarsih Gg. Situ Aksan Kota Bandung (Masjid Al-Furqon)',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (6, 3273160, 'Batununggal  ', 3, 'PC', NULL,
        'Jl. Maleer V No. 20 RT. 02/01 Kelurahan Maleer Kec. Batununggal Kota Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (7, 3273030, 'Bojongloa Kaler  ', 3, 'PC', NULL, 'Jl. Babakan Tarogong Gg. Lakasana No. 25 Kota Bandung', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (8, 3273040, 'Bojongloa Kidul  ', 3, 'PC', NULL, 'D/a. Mesjid Miftahul Jannah Jl. Cibaduyut No. 145 Bandung',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (9, 3273142, 'Mandalajati', 3, 'PC', NULL, 'Jl. Jatihandap No. 90/143 Mandalajati Bandung 40125', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (10, 3273210, 'Cibeunying Kidul', 3, 'PC', NULL, 'Jl. PHH. Mustapa Gg. Sukapada No. 10  Bandung 40125', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (11, 3273150, 'Kiaracondong  ', 3, 'PC', NULL, 'Jl. Kebon Kangkung 4 Kota Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (12, 3273060, 'Regol', 3, 'PC', NULL, 'Jl. Bpk. Ilus No. 115/18 B Bandung 40251', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (13, 3273240, 'Sukajadi', 3, 'PC', NULL, 'Jl. Sukagalih Gg. H. Ghozali Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (14, 3273250, 'Sukasari  ', 3, 'PC', NULL, 'Jl. Cipedes No. 37/176A Bandung 40173', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (15, 3273170, 'Sumurbandung  ', 3, 'PC', NULL, 'Gg. Sukareja No. 99A/34B Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (16, 3273080, 'Bandung Kidul', 3, 'PC', NULL,
        'D/a. Jl. Trs. Buah Batu No. 290 Kujangsari Bandung Kidul Bandung 40284', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (17, 3273090, 'Buah Batu', 3, 'PC', NULL, 'Masjid Manbaul Huda Jl. Cijawura Girang III Kota Bandung', NULL, NULL,
        1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (18, 3273110, 'Cibiru  ', 3, 'PC', NULL, 'Jl. Cipadung No. 28 RT.02/15 Cipadung Cibiru Bandung', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (19, 3273010, 'Bandung Kulon  ', 3, 'PC', NULL, 'Jl. Cijerah No. 18 Bandung 40213 Tlp. 6017731', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (20, 3273190, 'Cicendo  ', 3, 'PC', NULL,
        'Mesjid At-Thohirie Jl. Kebonkawung Gg. Umar No.8  Kel. Pasirkaliki Cicendo Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (21, 3273220, 'Cibeunying Kaler', 3, 'PC', NULL, 'Masjid Al-Ishlah
Jl. Sukamantri I RT.03/10 Bandung 40113 Tlp. (022) 2530247 (Asep) ', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (22, 3273200, 'Bandung Wetan', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (23, 3204, 'Kab. Bandung  ', 3, 'PD', NULL,
        'Jl. Raya Banjaran Kp. Langonsari RT. 01/04 Kec. Pameungpeuk Kab. Bandung Blk Pesantren Persis 3 Pameungpeuk',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (24, 3204140, 'Baleendah  ', 3, 'PC', NULL,
        'Jl. Terusan Siliwangi No. 42 A Cipicung Kec. Manggahang Kec. Baleendah Kab. Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (25, 3204160, 'Banjaran  ', 3, 'PC', NULL,
        'Jl. Pajagalan No. 115 (PPI 31) Desa Banjaran Kec. Banjaran Kab. Bandung 40377', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (26, 3204010, 'Ciwidey  ', 3, 'PC', NULL, 'Griya Sukasari RT. 01/18 Ds./Kec. Ciwidey Kab. Bandung', NULL, NULL,
        1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (27, 3204280, 'Bojongsoang  ', 3, 'PC', NULL, 'Jl. Ciganitri No. 2 Bojongsoang Kab. Bandung', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (28, 3204270, 'Dayeuhkolot  ', 3, 'PC', NULL,
        'Jl. Raya Dayeuhkolot KM 8 Gg. Harmaen No. 8 Kaum Dayeuhkolot Kab. Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (29, 3204130, 'Ciparay  ', 3, 'PC', NULL, 'Jl. Moh Ramdan Ds. Mekarsari Kec. Ciparay Kab.Bandung', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (30, 3204170, 'Pameungpeuk  ', 3, 'PC', NULL, 'Jl. Raya Banjaran Kp. Langonsari Kec. Pameungpeuk Kab. Bandung',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (31, 3204110, 'Rancaekek  ', 3, 'PC', NULL, 'Kp. Jambuleutik–Linggar Rancaekek Kab. Bandung 40394', NULL, NULL,
        1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (32, 3204190, 'Soreng', 3, 'PC', NULL,
        'Jl. Raya Soreang Belakang No. 121 Soreang Kab. Bandung 40911 Tlp. (022) 5891937', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (33, 3204191, 'Kutawaringin  ', 3, 'PC', NULL, 'Kp. Muara Ds. Kopo Soreang Kab. Bandung 40951', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (34, 3204250, 'Margaasih  ', 3, 'PC', NULL,
        'Jl. Mahmud 271 Kp. Curug RT. 04/08 Desa Rahayu Kec. Margaasih Kab. Bandung 40218', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (35, 3204180, 'Katapang  ', 3, 'PC', NULL,
        'Jl. Kopo Blk. No. 523 Bojong Buah Katapang Kab. Bandung 40971 Tlp.(022) 5892093 ', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (36, 3204040, 'Pangalengan  ', 3, 'PC', NULL,
        'Jl. PTPN VIII Kp. Cipanas RT. 03/05 Desa Margamukti Kec. Pangalengan Kab. Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (37, 3204150, 'Arjasari  ', 3, 'PC', NULL, 'Kp. Rancakole Kulon Ds. Rancakole Kec. Arjasri Kab. Bandung', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (38, 3204100, 'Cicalengka  ', 3, 'PC', NULL,
        'Jl. Raya Timur Km 33 Warung Lahang Nagrog Kec. Cicalengka Kab. Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (39, 3204080, 'Paseh  ', 3, 'PC', NULL,
        'Jl. Olah Raga Ds. Sukamantri RT.03/03 Kec. Paseh Majalaya Kab. Bandung 40383 Tlp. (022) 5951716-5953961', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (40, 3204260, 'Margahayu', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (41, 3204030, 'Cimaung', 3, 'PC', NULL,
        'Jl. Pangalengan Km 27 Kp. Pangkalan RT. 02/08 Desa Cipinang Kec. Cimaung Kab. Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (42, 3204050, 'Kertasari', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (43, 3204290, 'Cileunyi', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (44, 3204161, 'Cangkuang', 3, 'PC', NULL, 'Jl. Cirengit RT. 03/03 Desa Tanjungsari Kec. Cangkuang Kab. Bandung',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (45, 3204310, 'Cimenyan', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (46, 3204020, 'Pasirjambu', 3, 'PC', NULL, 'Pasanggrahan Ds./Kec. Pasirjambu Kab. Bandung', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (47, 3204120, 'Majalaya', 3, 'PC', NULL, 'Kp. Cangkuang RT. 03/05 Desa Biru Kec. Majalaya Kab. Bandung', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (48, 3204011, 'Rancabali', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (49, 3203, 'Kab. Cianjur  ', 3, 'PD', NULL, 'Jl. DR. Muwardi No. 171 C Cianjur ', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (50, 3203200, 'Cianjur  ', 3, 'PC', NULL,
        'Jl. Dr. Muwardi No. 171 C RT. 01/18 Desa Bojongherang Kec. Cianjur Kab. Cianjur 43218', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (51, 3203170, 'Ciranjang  ', 3, 'PC', NULL, 'Jl. Almaarif No. 143 Ciranjang Cianjur', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (52, 3203120, 'Cibeber  ', 3, 'PC', NULL, 'Jl. Raya Songgom No. 1 Cibeber Cianjur 43262', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (53, 3203240, 'Cikalong Kulon  ', 3, 'PC', NULL,
        'Kp. Babakan Loa RT.1/1 Ds. Majalaya Kec. Cikalong Kulon Kab. Cianjur', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (54, 3203230, 'Sukaresmi  ', 3, 'PC', NULL, 'Kp. Salagombong RT. 21/8 Ds. Sukaresmi Kec. Sukaresmi Kab. Cianjur',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (55, 3203160, 'Bojong Picung  ', 3, 'PC', NULL,
        'D/a. Mesjid Al-Miftah Ds. Neglasari Bj. Picung Cianjur 43283Tlp. (0263) 321352', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (56, 3203140, 'Cilaku  ', 3, 'PC', NULL, 'Kp. Bobojong RT. 04/01 Ds. Ciharashas Kec. Cilaku Kab. Cianjur', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (57, 3203220, 'Pacet', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (58, 3203161, 'Haurwangi', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (59, 3203090, 'Sukanagara', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (60, 3203190, 'Karang Tengah', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (61, 3271, 'Kota Bogor  ', 3, 'PD', NULL,
        'd/a. Pesantren Persis 112 Bogor, Jl. Raya Pajajaran No. 28 Kelurahan Bantarjati Kecamatan Bogor Utara Kota Bogor 16153 ',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (62, 3271030, 'Bogor Utara  ', 3, 'PC', NULL,
        'Jl. Raya Pajajaran No. 28 Blk. PB. Rimba Bogor 16153 Tlp. (0251) 338475', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (63, 3271040, 'Bogor Tengah', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (64, 3201, 'Kab. Bogor  ', 3, 'PD', NULL, 'Jl. R.E. Sulaeman No. 3 Kebon Kopi Citeureup Bogor', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (65, 3201200, 'Citeureup  ', 3, 'PC', NULL, 'Jl. R.E. Sulaeman No. 3 Kebon Kopi Citeureup Kab. Bogor', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (66, 3201210, 'Cibinong', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (67, 3214, 'Kab. Purwakarta  ', 3, 'PD', NULL, 'Jl. R.E. Martadinata No. 27 Purwakarta 41112', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (68, 3214100, 'Purwakarta  ', 3, 'PC', NULL, 'Jl. R.E. Martadinata No. 27 Purwakarta 41112', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (69, 3214040, 'Plered  ', 3, 'PC', NULL, 'Jl. Raya Warung Kandang Desa Plered Kab. Purwakarta', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (70, 3213, 'Kab. Subang  ', 3, 'PD', NULL, 'Jl. Raya Cicadas RT. 02/01 Desa Cadas Kec. Binong Kab. Subang 41253',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (71, 3213210, 'Legonkulon  ', 3, 'PC', NULL,
        'D/a. Mesjid Al-Istiqomah. Kp. Anjun Ds. Legonkulon Permat Anjun Subang 41254', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (72, 3213200, 'Pusakanagara  ', 3, 'PC', NULL,
        'D/a. Mesjid Al-Huda Jl. Selatan Pusakanagara KM. 04  Pusakanagara Subang 41255', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (73, 3213190, 'Pamanukan  ', 3, 'PC', NULL, 'D/a. Pesantren Persis No. 51 Pangadangan- Pamanukan Subang', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (74, 3213140, 'Pagaden  ', 3, 'PC', NULL, 'Jl. Stasion No. 125 Blk. SMPN I  Pagaden Subang 41252', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (75, 3205, 'Kab. Garut  ', 3, 'PD', NULL, 'Jl. Guntur Melati No. 13 Kec. Haurpanggung Kab. Garut 44151', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (76, 3205210, 'Wanaraja  ', 3, 'PC', NULL, 'D/a. Sdr. Rijaludin Kp. Babakan Loa - Wanaraja Garut 44183 ', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (77, 3205160, 'Cisurupan  ', 3, 'PC', NULL, 'Cisero-Cisurupan Garut 44163', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (78, 3205120, 'Cikajang  ', 3, 'PC', NULL, 'Jl. Pasar Kulon No. 6 Cikajang Garut 44171', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (79, 3205190, 'Garut Kota  ', 3, 'PC', NULL, 'Jl. Guntur No. 165 A Bentar Garut 44115', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (80, 3205170, 'Samarang  ', 3, 'PC', NULL, 'Jl. Pasirwangi - Garogol Garut 44115', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (81, 3205181, 'Tarogong', 3, 'PC', NULL, 'D/a. Mesjid “ Ihyau Al-Islam” Jl. Pembangunan No. 79 Tarogong Garut',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (82, 3205140, 'Cilawu  ', 3, 'PC', NULL, 'D/a. Jl. Guntur No. 165 A Bentar Garut 44115', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (83, 3205260, 'Cibatu  ', 3, 'PC', NULL,
        'D/a. Pesantren Persis No. 81 Cibatu Kp. Cikajang Ds. Mekarsari Cibatu-Garut 44185', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (84, 3205261, 'Kersamanah  ', 3, 'PC', NULL, 'Jl. Raya Kurnia No. 56 Kersamanah-Garut 44185', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (85, 3205200, 'Karangpawitan  ', 3, 'PC', NULL, 'D/a. PC. Persis Karangpawitan Jl. Karangpawitan No. 53 Garut',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (86, 3205050, 'Depok Pakenjeng', 3, 'PC', NULL,
        'D/a. PC. Persis Pakenjeng Ds. Depok Pakenjeng Bungbulang Garut 44165', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (87, 3205150, 'Bayongbong  ', 3, 'PC', NULL, 'Kp. Cioyod RT. 04/02 Mekarjaya Bayongbong-Garut 44162', NULL, NULL,
        1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (88, 3205250, 'Leuwigoong  ', 3, 'PC', NULL,
        'D/a. Masjid Al-Irsyad Kp. Lio RT. 03/03 Ds. Tambaksari Kec. Leuwigoong Kab.Garut', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (89, 3205211, 'Sucinaraja', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (90, 3205212, 'Pangatikan', 3, 'PC', NULL,
        'Jl Sukaraja No. 269 RT. 01/05 Desa Babakanloa Kec. Pangatikan Kab. Garut 44183', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (91, 3205182, 'Tarogong Kaler', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (92, 3205171, 'Pasirwangi  ', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (93, 3205130, 'Banjarwangi', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (94, 3205070, 'Pameungpeuk', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (95, 3205230, 'Banyuresmi', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (96, 3205221, 'Karang Tengah', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (97, 3206, 'Kab. Tasikmalaya  ', 3, 'PD', NULL, 'Jl. Cempakawarna No. 75 Tasikmalaya 46123', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (98, 3206260, 'Ciawi  ', 3, 'PC', NULL, 'D/a. Dadang Juhana Jl. Raya Utara No. 230 Ciawi Tasikmalaya 46123 ',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (99, 3206261, 'Kadipaten  ', 3, 'PC', NULL, 'Kp. Cirando Ds. Kadipaten Ciawi Tasikmalaya 45156', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (100, 3206271, 'Sukaresik', 3, 'PC', NULL,
        'D/a. Pc. Persis Pagerageung Kp. Mekarsari - Tanjungsari Pagerageung Tasikmalaya 46159 ', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (101, 3206250, 'Jamanis  ', 3, 'PC', NULL,
        'Jl. Raya Ciawi-Tasikmalaya KM. 3 Tasikmalaya 46175 Tlp.(0265) 455437 ', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (102, 3206240, 'Rajapolah  ', 3, 'PC', NULL, 'Jl. Ciberekah No. 60 Rajapolah Tasikmalaya 46155', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (103, 3207, 'Kab. Ciamis  ', 3, 'PD', NULL, 'Jl. RTA Sunarya Kalapa Jajar Ciamis', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (104, 3207220, 'Cikoneng  ', 3, 'PC', NULL, 'Dusun Kujang RT. 03/02 Desa Kujang Kec. Cikoneng Kab. Ciamis 46261',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (105, 3207100, 'Banjarsari', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (106, 3207291, 'Sukamantri  ', 3, 'PC', NULL,
        'Jl. Panji Siliwangi Kp. Caringin RT. 35/12 Desa Cibeureum Kec. Sukamantri Kab. Ciamis 46264', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (107, 3207221, 'Sindangkasih', 3, 'PC', NULL, 'Jl. H Makbul Dusun Kadupugur Desa Gunungapu Sindangkasih Ciamis',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (108, 3207210, 'Ciamis Kota', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (109, 3207300, 'Panumbangan', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (110, 3210, 'Kab. Majalengka  ', 3, 'PD', NULL,
        'd/a Pesantren Persis 138 Cikijing Jl. Harapan Mukti No. 10 RT. 01/02 Desa Sindang Kec. Cikijing Kab. Majalengka 45466',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (111, 3210070, 'Majalengka  ', 3, 'PC', NULL, 'Jl. Emen Slamet No. 260 Majalengka 45413', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (112, 3210140, 'Jatiwangi  ', 3, 'PC', NULL, 'Jl. Lanud S. Sukani No. 817 Ds.Jum’at Jatiwangi Majalengka 45454',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (113, 3210210, 'Sumberjaya  ', 3, 'PC', NULL,
        'Jl. Pahlawan I No. 828 Panjalin Kidul-Sumberjaya Majalengka 45455', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (114, 3210030, 'Cikijing  ', 3, 'PC', NULL, 'Jl. Harapan Mukti No. 10 Sindang Cikijing Majalengka', NULL, NULL,
        1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (115, 3210031, 'Cingambul', 3, 'PC', NULL,
        'd/a Masjid Muallimin Jl. Raya Cikijing-Ciamis Blok Mulyasari RT. 01/01 Desa Ciranjeng Kec. Cingambul Kab. Majalengka 45467',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (116, 3210170, 'Kadipaten', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (117, 3210190, 'Jati Tujuh', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (118, 3202, 'Kab. Sukabumi  ', 3, 'PD', NULL, 'Jl. Tegalpadul II No. 79 Sukabumi 43113', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (119, 3272030, 'Warudoyong  ', 3, 'PC', NULL, 'D/a. Pesantren Persis Jl. Raya Cisereh No. 1 Sukabumi', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (120, 3202170, 'Sukaraja  ', 3, 'PC', NULL, 'D/a. Asep (DKM. Al-Ghifari) Jl. Salabintana KM. 7 Sukabumi', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (121, 3202200, 'Cisaat', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (122, 3277, 'Kota Cimahi  ', 3, 'PD', NULL,
        'D/a. Pesantren Persis Melong Jl. Melong Blk. Cikendal RT.03/V Melong Cimahi Selatan Bandung', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (123, 3277010, 'Cimahi Selatan', 3, 'PC', NULL,
        'Melong Cikendal RT. 05/05 Keluarahan Melong Kec. Cimahi Selatan Kota Cimahi', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (124, 3277030, 'Cimahi Utara  ', 3, 'PC', NULL,
        'D/a. Engkos Kosasih Kp. Bobojong (Depan SMPN 5 Cimahi) Kota Cimahi', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (125, 3277020, 'Cimahi Tengah  ', 3, 'PC', NULL,
        'Jl. Pojok Selatan RT. 02/07 Keluarahan Setiamanah Kec. Cimahi Tengah Kota Cimahi', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (126, 3211, 'Kab. Sumedang  ', 3, 'PD', NULL, 'Jl. Prabu Geusang Ulun Gg. Al-Furqon No.72 Kab. Sumedang', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (127, 3211030, 'Tanjungsari', 3, 'PC', NULL,
        'Mesjid Ashabul Kahfi  Jl. Raya Tanjungsari No. 191 Blk. Toko Mebel Jaya Abadi Tanjungsari Sumedang', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (128, 3211010, 'Jatinangor  ', 3, 'PC', NULL,
        'D/a. Rumah Makan Sukahati Jl. Raya Cipacing KM. 20 Arpus Cikeruh Sumedang', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (129, 3211050, 'Sumedang Selatan', 3, 'PC', NULL, 'Jl. Prabu Geusan Ulun Gg. Al-Furqon No. 72 Kab. Sumedang',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (130, 3211032, 'Pamulihan', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (131, 3211150, 'Paseh', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (132, 3211040, 'Rancakalong', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (133, 3211020, 'Cimanggung', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (134, 3278, 'Kota Tasikmalaya  ', 3, 'PD', NULL,
        'Jl. Cempakawarna No. 75 RT. 03/08 Cilembang Cihideung Kota Tasikmalaya', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (135, 3278050, 'Cihideung  ', 3, 'PC', NULL,
        'Jl. Cempakawarna No. 75 Rt. 03/08 Cilembang Cihideung Kota Tasikmalaya', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (136, 3278080, 'Cipedes  ', 3, 'PC', NULL, 'Jl. RE. Martadinata No. 95 Kota Tasikmalaya', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (137, 3278070, 'Indihiang  ', 3, 'PC', NULL, 'Sukarindik Kel. Sukarindik Kec. Bungursari Kota Tasikmalaya', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (138, 3278060, 'Mangkubumi', 3, 'PC', NULL,
        'Jl. AH Nasution Labuhan Bulan Kel./Kec. Mangkubumi Kota Tasikmalaya 46181 (Masjid An Nahl)', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (139, 3217, 'Kab. Bandung Barat', 3, 'PD', NULL,
        'Jl. Karyabakti Kp. Sukamulya RT. 02/25 Ds./Kec. Padalarang Kab. Bandung Barat 40553', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (140, 3217090, 'Padalarang  ', 3, 'PC', NULL,
        'Jl. Karyabakti Kp. Sukamulya RT. 02/25 Ds./Kec. Padalarang Kab. Bandung Barat 40553', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (141, 3217080, 'Cipatat  ', 3, 'PC', NULL,
        'Jl. Raya Cipatat KM 24 (Masjid Al-Furqon) Desa Cipatat Kec. Cipatat Kab. Bandung Barat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (142, 3217040, 'Cililin  ', 3, 'PC', NULL,
        'Kp. Cigandasoli RT. 03/06 Desa Karanganyar Kec. Cililin Kab. Bandung Barat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (143, 3217070, 'Batujajar  ', 3, 'PC', NULL, 'Kp. Pajajaran Ds. Pangauban Kec. Batujajar Kab. Bandung Barat',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (144, 3217120, 'Lembang  ', 3, 'PC', NULL, 'Jl. Kayu Ambon No. 36 Kec. Lembang Kab. Bandung Barat 40391', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (145, 3217140, 'Cikalong Wetan  ', 3, 'PC', NULL,
        'Jl. Cisomang RT. 03/09 Desa Cisomang Barat Kec. Cikalong Wetan Kab. Bandung Barat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (146, 3217050, 'Cihampelas', 3, 'PC', NULL,
        'Jl. Pajagalan No. 3 Ds. Mekarmukti Kecamatan Cihampelas Kab. Bandung Barat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (147, 3217100, 'Ngamprah', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (148, 3217150, 'Cipeundeuy', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (149, 3217020, 'Gununghalu', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (150, 3279, 'Kota Banjar', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (151, 3279010, 'Banjar', 3, 'PC', NULL, 'Jl. Batulawang No. 47 A Banjar Ciamis 46322', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (152, 3274010, 'Harjamukti', 3, 'PC Non PD', NULL,
        'Jl. Ahmad Yani (By Pass) No. 5 Larangan Kecamatan Harjamukti Kota Cirebon', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (153, 3215150, 'Rengasdengklok', 3, 'PC Non PD', NULL,
        'Jl.Pasar Rengasdengklok Gg.H.Parna No.10 Warudoyong Rengasdengklok 41351Tlp.(0267)483380', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (154, 3275040, 'Bekasi Timur', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (155, 3216061, 'Cikarang Utara', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (156, 3276020, 'Pancoran Mas', 3, 'PC Non PD', NULL,
        'Jl. Kemuliaan RT.6/01 No. 54 B Mekar Jaya - Sukma Jaya Depok II Tengah 16411 Tlp. (021) 77822942-7756357',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (157, 3276030, 'Sukmajaya', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (158, 31, 'DKI Jakarta  ', 3, 'PW', NULL, 'Jl. Johar Baru I No. 22 Jakarta Pusat 10560 Tlp. (021) 4249111', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (159, 3173, 'Kota Jakarta Pusat  ', 3, 'PD', NULL,
        'Kp. Rawa Sawah II RT. 08/06 Kel. Kampung Rawa Kec. Johar Baru Jakarta Pusat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (160, 3173040, 'Johar Baru', 3, 'PC', NULL, 'Jl. Mardani Raya Gg. S No. 2 Johar Baru Jakarta Pusat 10560', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (161, 3173030, 'Senen  ', 3, 'PC', NULL, 'D/a. Jl. Johar Baru I No. 21 Jakarta Pusat', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (162, 3174, 'Kota Jakarta Barat  ', 3, 'PD', NULL,
        'Jl. Empang Bahagia Raya No. 1 Kel. Jelembar Jakarta Barat 11460', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (163, 3174040, 'Grogol Petamburan', 3, 'PC', NULL,
        'Jl. Empang Bahagia Raya No. 1 Kel. Jelembar Jakarta Barat 11460', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (164, 3174070, 'Cengkareng  ', 3, 'PC', NULL,
        'Jl. Akasia 5 RT. 15/12 Cengkareng Timur Cengkareng Jakarta Barat DKI Jakarta', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (165, 3174080, 'Kalideres  ', 3, 'PC', NULL,
        'Jl.  Sumbawa  III  No.  2  RT. 08/06  Kav.  PTB  Tegal  Alur Kalideres Jakarta 11820', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (166, 3172, 'Kota Jakarta Timur  ', 3, 'PD', NULL,
        'D/a. Pesantren Persis Matraman Jl. Kramat Asem Raya No. 59 Jakarta Timur 13310', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (167, 3172060, 'Jatinegara  ', 3, 'PC', NULL,
        'Mesjid Al-Istiqomah Pst. Jatinegara Jl. Raya Selatan Jakarta Timur 13310', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (168, 3172100, 'Matraman  ', 3, 'PC', NULL, 'Jl. Kramat asem No. 59 Jakarta Timur 13120', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (169, 3175, 'Kota Jakarta Utara  ', 3, 'PD', NULL, 'Jl. Yos Sudarso Lorong 102/56 Jakarta Utara 14220', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (170, 3175040, 'Koja  ', 3, 'PC', NULL, 'Jl. Lorong No. 103/56 Jakarta Utara', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (171, 3175030, 'Tanjungpriok  ', 3, 'PC', NULL, 'Jl. Lorong No. 103/56 Jakarta Utara', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (172, 3175020, 'Pademangan  ', 3, 'PC', NULL, 'Jl. Pademangan IV Gg. 7 No. 9 RT.05/08  Jakarta Utara 14410',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (173, 3171, 'Kota Jakarta Selatan', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (174, 3171100, 'Setiabudi', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (175, 36, 'Banten', 3, 'PW', NULL,
        'Jl. Empat lima  Ruko Singandaru No. 1 (Samping TPU Singandaru) Kaujon Singandaru RT. 001/022 Kel./Kec. Serang, Kota Serang 42116',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (176, 3673040, 'Serang', 3, 'PC Non PD', NULL,
        'Jl. Banten Kebaharan Masjid Al-Manar RT. 08/03 Kel. Lopang Kec. Serang, Kota Serang 42113', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (177, 3604020, 'Padarincang', 3, 'PC Non PD', NULL,
        'Jl. Palka Km. 37 Kp. Gunung Buntung, Ds. Kramatlaban, Kec. Padarincang, Kab. Serang 42168', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (178, 3602180, 'Rangkasbitung', 3, 'PC Non PD', NULL,
        'Jl. Ir H juanda No. 16 L, Komplek Pendidikan Kel. Muara Ciujung Timur Kec. Rangkasbitung Kab. Lebak 42314',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (179, 3671011, 'Larangan', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (180, 75, 'Gorontalo', 3, 'PW', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (181, 7502, 'Kab. Gorontalo  ', 3, 'PD', NULL,
        'Jl. Kihajar Dewantoro No. 69 Gorontalo Limboto Tlp. (0435) 881474 - 881465', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (182, 7504, 'Kab. Bone Bolango', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (183, 7503, 'Kab. Pohuhato', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (184, 7571, 'Kota Gorontalo', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (185, 7571020, 'Kota Selatan  ', 3, 'PC', NULL,
        'Jl. Rajaeyatu No. 18 Kl. Biawao Kota Selatan Kota Gorontalo Provinsi Gorontalo 96111', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (186, 35, 'Jawa Timur  ', 3, 'PW', NULL, 'Jl. Babatan Pilang II/2 Surabaya 60227', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (187, 3528, 'Kab. Pamekasan', 3, 'PD', NULL, 'Jl. Peayaman Gg.I No. 5 Pamekasan Madura 69315', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (188, 3528, 'Pamekasan', 3, 'PC', NULL, 'Jl. Peayaman GG.I No. 5 Pamekasan Madura 69315 Tlp. (0324) 21407', NULL,
        NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (189, 3529, 'Kab. Sumenep', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (190, 3529230, 'Sapeken  ', 3, 'PC', NULL,
        'D/a. Pesantren Abu Huraeroh No. 95 Abu Huraeroh Pulau Sapeken Sumenep-Madura 69493', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (191, 3529030, 'Saronggi  ', 3, 'PC', NULL, 'Jl. Raya Saronggi No. 48 Saronggi Sumenep Madura', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (192, 3529120, 'Abunten  ', 3, 'PC', NULL,
        'Jl. Mandaraga Kp. Mandala Ds. Keles RT. 06/03 Abunten Sumenep-Madura 69455', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (193, 3529070, 'Sumenep  ', 3, 'PC', NULL, 'Jl. DR. Wahidin No. 38 Pajagalan Sumenep Madura Tlp. (0328) 65124',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (194, 3528010, 'Tlanakan', 3, 'PC Non PD', NULL,
        'Jl. Madrasah Al-Amin No. 3 Branta Pesisir Kec. Tlanakan Pamekasan Madura 69311', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (195, 5206, 'Kab. Bima', 3, 'PD', NULL, 'Jl. Pendidikan No. 10 Raba-Bima NTB Tlp. (0374) 42633', NULL, NULL, 1,
        NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (196, 5206040, 'Belo  ', 3, 'PC', NULL, 'Jl. Pendidikan No. 10 Raba -Bima (NTB) 84116', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (197, 5272010, 'Rasanae', 3, 'PC', NULL, 'Jl. Pendidikan No. 10 Raba -Bima (NTB) 84116', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (198, 5206060, 'Sape  ', 3, 'PC', NULL, 'Jl. Lap-Puaih Desa Rasabau Sape Bima NTB 84182', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (199, 5206070, 'Wera  ', 3, 'PC', NULL, 'Jl. Pendidikan No. 10 Raba -Bima (NTB) 84116', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (200, 5206030, 'Woha  ', 3, 'PC', NULL, 'Jl. Pendidikan No. 10 Raba -Bima (NTB) 84116', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (201, 72, 'Sulawesi Tengah  ', 3, 'PW', NULL, 'Jl. Danau Telaga No. 9 Palu Sulawesi Tengah Tlp. (0451) 21891',
        NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (202, 12, 'Sumatera Utara', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (203, 1219020, 'Tanjung Tiram', 3, 'PC Non PD', NULL,
        'Jl. Merdeka No. 84 Dusun I Ds. Sukamaju Kec. Tanjungtiram Asahan Sumatera Utara 21253', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (204, 1207130, 'Bilah Hilir', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (205, 1209190, 'Perdagangan', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (206, 1706020, 'Pondok Suguh', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (207, 64, 'Kalimantan Timur', 3, 'Perwakilan PW', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (208, 18, 'Lampung', 3, 'Perwakilan PW', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (209, 1806, 'Kab. Lampung Utara', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (210, 1806071, 'Muara Sungkai', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (211, 1805, 'Kab. Lampung Tengah', 3, 'PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (212, 1805013, 'Anak Tuha', 3, 'PC', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (213, 1801063, 'Kebun Tebu', 3, 'PC Non PD', NULL, '', NULL, NULL, 1, NULL);
INSERT INTO t_master_jamiyyah (id_jamiyyah, id_wilayah, nama_pimpinan, id_otonom, pimpinan, nomor, alamat, latitude,
                               longitude, status_aktif, deleted_at)
VALUES (214, 51, 'Bali', 3, 'PW', NULL, '', NULL, NULL, 1, NULL);