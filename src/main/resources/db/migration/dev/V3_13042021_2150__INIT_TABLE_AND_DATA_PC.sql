CREATE TABLE t_pc
(
    kd_pc      varchar(10)                         NOT NULL
        PRIMARY KEY,
    kd_pd      varchar(6)                          NOT NULL,
    nama_pc    varchar(191)                        NOT NULL,
    diresmikan date                                NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC001', 'PD001', 'Anggota Tersiar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC002', 'PD009', 'Cianjur', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC003', 'PD009', 'Ciparay', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC004', 'PD001', 'Paseh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC005', 'PD018', 'Padalarang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC006', 'PDXYZ', 'Purwakarta', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC007', 'PD024', 'Tanjung Priuk', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC008', 'PD001', 'Pameungpeuk', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC009', 'PD001', 'Rancaekek', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC010', 'PD002', 'Cibeunying Kidul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC011', 'PD002', 'Bandung Kulon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC012', 'PD002', 'Astanaanyar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC013', 'PD007', 'Garut Kota', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC014', 'PD002', 'Bojongloa Kaler', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC015', 'PD003', 'Sukaresik', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC016', 'PD001', 'Banjaran', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC017', 'PD007', 'Samarang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC018', 'PD001', 'Soreang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC019', 'PD005', 'Cimahi Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC020', 'PD003', 'Cihideung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC021', 'PD001', 'Baleendah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC022', 'PD020', 'Pamanukan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC023', 'PD001', 'Kutawaringin', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC024', 'PDXYZ', 'Plered', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC025', 'PD005', 'Cimahi Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC026', 'PD001', 'Margaasih', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC027', 'PD003', 'Rajapolah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC028', 'PD003', 'Ciawi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC029', 'PD003', 'Cipedes', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC030', 'PD001', 'Dayeuh Kolot', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC031', 'PD001', 'Bojong Soang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC032', 'PD019', 'Cirebon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC033', 'PD001', 'Katapang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC034', 'PDXYZ', 'Pamekasan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC035', 'PDXYZ', 'Tlanakan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC036', 'PDXYZ', 'Sapeken', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC037', 'PDXYZ', 'Serang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC038', 'PDXYZ', 'Bogor Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC039', 'PD022', 'Matraman', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC040', 'PD002', 'Bojongloa Kidul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC041', 'PD002', 'Batununggal', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC042', 'PD002', 'Regol', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC044', 'PD002', 'Babakan Ciparay', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC045', 'PD001', 'Ciwidey', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC046', 'PDXYZ', 'Muko-muko Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC047', 'PD003', 'Jamanis', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC048', 'PDXYZ', 'Ambuten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC049', 'PDXYZ', 'Saronggi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC050', 'PD022', 'Jatinegara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC051', 'PDXYZ', 'Padarincang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC052', 'PD05', 'Cimahi Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC053', 'PD018', 'Cililin', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC054', 'PD008', 'Cibeber', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC055', 'PD004', 'Gunung Puyuh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC056', 'PD004', 'Sukaraja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC057', 'PD017', 'Cikoneng', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC058', 'PDXYZ', 'Bangil', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC059', 'PD025', 'Johar Baru', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC060', 'PD009', 'Tanjung Sari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC061', 'PD011', 'Kota Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC062', 'PD002', 'Andir', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC063', 'PD021', 'Majalengka', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC064', 'PD008', 'Ciranjang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC065', 'PD007', 'Tarogong Kidul', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC066', 'PD011', 'Kota Utara', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC067', 'PD002', 'Sukasari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC068', 'PD009', 'Jatinangor', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC069', 'PD007', 'Banyuresmi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC070', 'PD002', 'Sukajadi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC071', 'PD018', 'Cipatat', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC072', 'PDXYZ', 'Depok', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC073', 'PD002', 'Cibiru', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC074', 'PD017', 'Banjarsari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC075', 'PD019', 'Sumedang Selatan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC076', 'PD018', 'Cikalong Wetan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC077', 'PD001', 'Arjasari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC079', 'PD007', 'Kersamanah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC080', 'PD018', 'Cikalong Kulon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC081', 'PD003', 'Kadipaten', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC082', 'PD001', 'Pangalengan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC083', 'PD010', 'Mangkubumi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC084', 'PD010', 'Tawang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC085', 'PD010', 'Indihiang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC086', 'PD020', 'Legon Kulon', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC087', 'PD002', 'Buahbatu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC088', 'PDXYZ', 'Rangkasbitung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC089', 'PD018', 'Batujajar', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC090', 'PD007', 'Cikajang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC091', 'PD009', 'Pamulihan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC092', 'PDXYZ', 'Tanjung Tiram', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC093', 'PD017', 'Sindang Kasih', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC094', 'PDXYZ', 'Citeureup', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC095', 'PD016', 'Cikarang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC096', 'PD002', 'Arcamanik', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC097', 'PD001', 'Cangkuang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC098', 'PD018', 'Cihampelas', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC099', 'PD001', 'Cileunyi', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC100', 'PD007', 'Cibatu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC101', 'PD001', 'Rancabali', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC102', 'PDXYZ', 'Kuningan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC103', 'PD019', 'Harjamukti', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC104', 'PD019', 'Lemah Wungkuk', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC105', 'PD020', 'Pagaden', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC106', 'PD001', 'Pasir Jambu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC107', 'PD020', 'Pusaka Jaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC108', 'PD010', 'Cigalontang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC109', 'PD021', 'Sumber Jaya', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC110', 'PD021', 'Sindang / Maja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC111', 'PD022', 'Cipayung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC112', 'PD023', 'Cengkareng', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC113', 'PD023', 'Grogol', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC114', 'PD024', 'Pademangan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC116', 'PD024', 'Koja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC117', 'PD004', 'Warudoyong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC119', 'PD018', 'Ngamprah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC120', 'PD001', 'Cimaung', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC121', 'PD007', 'Tarogong Kaler', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC122', 'PDXYZ', 'Perdagangan – Lima Puluh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC123', 'PD004', 'Cikole', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC124', 'PD007', 'Wanaraja', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC125', 'PD002', 'Bandung Wetan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC126', 'PDXYZ', 'Benai', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC127', 'PD017', 'Panumbangan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC128', 'PD017', 'Ciamis Kota', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC129', 'PD018', 'Lembang', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC130', 'PD001', 'Paseh', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC131', 'PD001', 'Kertasari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC132', 'PD007', 'Cisurupan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC133', 'PD007', 'Pangatikan', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC134', 'PD007', 'Pameungpeuk', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC135', 'PD007', 'Pakenjeng', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC136', 'PD007', 'Leuwigoong', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC137', 'PD007', 'Leles', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC138', 'PD007', 'Cimanggis', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC139', 'PD002', 'Mandalajati', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC140', 'PD010', 'Bungursari', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC141', 'PDXYZ', 'Torgamba', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC142', 'PD018', 'Gunung Halu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC143', 'PDXYZ', 'Bogor Tengah', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC144', 'PD001', 'Cicalengka', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC145', 'PD001', 'Margahayu', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_pc (kd_pc, kd_pd, nama_pc, diresmikan, created_at, updated_at)
VALUES ('PC146', 'PD021', 'Cikijing', CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

