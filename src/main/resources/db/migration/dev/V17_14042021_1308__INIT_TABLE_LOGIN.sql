CREATE TABLE t_login
(
    id_login             varchar(32)  NOT NULL
        PRIMARY KEY,
    npa                  varchar(11)  NOT NULL,
    username             varchar(50)  NOT NULL UNIQUE,
    email                varchar(50)  NOT NULL UNIQUE,
    password             varchar(191) NOT NULL,
    activ_code           varchar(191) NOT NULL,
    email_confirm        int          NOT NULL,
    remember_token       varchar(100) NULL,
    reset_password_token varchar(100) NULL,
    status_aktif         int          NOT NULL,
    created_at           timestamp NULL,
    updated_at           timestamp NULL,
    CONSTRAINT t_login_npa_foreign
        FOREIGN KEY (npa) REFERENCES t_anggota (npa)
) COLLATE=utf8mb4_unicode_ci;

