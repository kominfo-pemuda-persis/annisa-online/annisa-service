CREATE TABLE t_master_otonom
(
    id_otonom   int auto_increment
        PRIMARY KEY,
    nama_otonom varchar(20) NOT NULL
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (1, 'Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (2, 'Persistri');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (3, 'Pemuda Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (4, 'Pemudi Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (5, 'HIMA Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (6, 'HIMI Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (7, 'HIMI Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (8, 'HIMI Persis');
INSERT INTO t_master_otonom (id_otonom, nama_otonom)
VALUES (9, 'Brigade Persis');