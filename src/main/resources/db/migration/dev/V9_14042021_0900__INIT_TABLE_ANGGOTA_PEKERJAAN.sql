CREATE TABLE t_anggota_pekerjaan
(
    id_pekerjaan  int auto_increment
        PRIMARY KEY,
    id_anggota    varchar(32)  NOT NULL,
    pekerjaan     varchar(130) NOT NULL,
    keterangan    varchar(255) NULL,
    alamat        varchar(255) NULL,
    tahun_mulai   int          NOT NULL,
    tahun_selesai int          NOT NULL,
    deleted_at    timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

