CREATE TABLE t_anggota_keterampilan
(
    id_keterampilan int auto_increment
        PRIMARY KEY,
    id_anggota      varchar(32)  NOT NULL,
    keterampilan    varchar(100) NOT NULL,
    deleted_at      timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

