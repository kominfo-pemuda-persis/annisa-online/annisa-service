CREATE TABLE t_master_pendidikan
(
    id_tingkat_pendidikan int auto_increment
        PRIMARY KEY,
    pendidikan            varchar(10) NOT NULL,
    deleted_at            timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (1, 'SD', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (2, 'MI', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (3, 'SMP', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (4, 'MTs', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (5, 'SMA', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (6, 'SMK', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (7, 'STM', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (8, 'MA', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (9, 'MLN', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (10, 'D1', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (11, 'D2', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (12, 'D3', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (13, 'D4', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (14, 'S1', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (15, 'S2', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (16, 'S3', NULL);
INSERT INTO t_master_pendidikan (id_tingkat_pendidikan, pendidikan, deleted_at)
VALUES (17, 'KOSONG', NULL);

