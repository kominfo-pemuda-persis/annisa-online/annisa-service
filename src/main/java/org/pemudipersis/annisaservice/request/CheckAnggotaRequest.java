package org.pemudipersis.annisaservice.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.10
 */
@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class CheckAnggotaRequest {

    @NotNull(message = "NPA can't be null")
    @Pattern(regexp = "\\d{2}.\\d{4}", message = "NPA harus dalam format xx.xxxx")
    String npa;
}
