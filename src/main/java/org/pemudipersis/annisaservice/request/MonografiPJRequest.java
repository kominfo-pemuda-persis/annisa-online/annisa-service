package org.pemudipersis.annisaservice.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.13
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MonografiPJRequest {

    private String kodePj;
    private String namaPj;
    private String kdPc;
    private String kdPd;
    private String kdPw;
    private String provinsi;
    private String kabupaten;
    private String kecamatan;
    private String desa;
    private double latitude;
    private double longitude;
    private String email;
    private String noTelpon;
    private String alamat;
    private String luas;
    private String bwTimur;
    private String bwBarat;
    private String bwSelatan;
    private String bwUtara;
    private String jarakProvinsi;
    private String jarakKabupaten;
    private String photo;
    private String ketua;
    private String sekretaris;
    private String bendahara;
    private String hariNgantor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Time waktuNgantor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp musjamTerakhirMasehi;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp musjamTerakhirHijriyyah;

    private int anggotaBiasa;
    private int anggotaLuarBiasa;
    private String tidakHerReg;
    private String mutasiPersis;
    private String mutasiTempat;
    private String mengundurkanDiri;
    private String meninggalDunia;
    private String calonAnggota;
}