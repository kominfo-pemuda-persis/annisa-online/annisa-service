package org.pemudipersis.annisaservice.service;

import lombok.extern.log4j.Log4j2;
import org.pemudipersis.annisaservice.entity.anggota.MasterPendidikan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Desa;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kecamatan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Provinsi;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.repository.jamiyyah.DesaRepository;
import org.pemudipersis.annisaservice.repository.jamiyyah.KabupatenRepository;
import org.pemudipersis.annisaservice.repository.jamiyyah.KecamatanRepository;
import org.pemudipersis.annisaservice.repository.jamiyyah.MasterPendidikanRepository;
import org.pemudipersis.annisaservice.repository.jamiyyah.ProvinsiRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.12
 */
@Service
@Log4j2
public class HelperUtilService {
    private final ProvinsiRepository provinsiRepository;
    private final KabupatenRepository kabupatenRepository;
    private final KecamatanRepository kecamatanRepository;
    private final DesaRepository desaRepository;
    private final MasterPendidikanRepository masterPendidikanRepository;

    public HelperUtilService(ProvinsiRepository provinsiRepository, KabupatenRepository kabupatenRepository,
                             KecamatanRepository kecamatanRepository, DesaRepository desaRepository,
                             MasterPendidikanRepository masterPendidikanRepository) {
        this.provinsiRepository = provinsiRepository;
        this.kabupatenRepository = kabupatenRepository;
        this.kecamatanRepository = kecamatanRepository;
        this.desaRepository = desaRepository;
        this.masterPendidikanRepository = masterPendidikanRepository;
    }

    public Provinsi getProvinsiById(String provinsiId) {
        Optional<Provinsi> provinsiOptional = provinsiRepository.findById(provinsiId);
        if (provinsiOptional.isPresent())
            return provinsiOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "82000", "Provinsi not found");
    }

    public Kabupaten getKabupatenById(String kabupatenId) {
        Optional<Kabupaten> kabupatenOptional = kabupatenRepository.findById(kabupatenId);
        if (kabupatenOptional.isPresent())
            return kabupatenOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "83000", "Kabupaten not found");
    }

    public Kecamatan getKecamatanById(String kecamatanId) {
        Optional<Kecamatan> kecamatanOptional = kecamatanRepository.findById(kecamatanId);
        if (kecamatanOptional.isPresent())
            return kecamatanOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "84000", "Kecamatan not found");
    }

    public Desa getDesaByid(String desaId) {
        Optional<Desa> desaOptional = desaRepository.findById(desaId);
        if (desaOptional.isPresent())
            return desaOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "85000", "Desa not found");
    }

    public MasterPendidikan getMasterPendidikanById(int idTingkatPendidikan) {
        return masterPendidikanRepository.findById(idTingkatPendidikan)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "MASTER_PENDIDIKAN_NOT_FOUND", "MASTER" +
                        " PENDIDIKAN NOT FOUND"));
    }
}
