package org.pemudipersis.annisaservice.service.mail;

import lombok.RequiredArgsConstructor;
import org.pemudipersis.annisaservice.config.MailConfig;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.46
 */
@Service
@RequiredArgsConstructor
public class MailService {
    private final MailConfig mailConfig;
    private final JavaMailSender javaMailSender;

    public void sendMail(MailMessageContent content) {
        SimpleMailMessage message = new SimpleMailMessage();
        String from = StringUtils.isEmpty(content.getFrom()) ? mailConfig.getFrom() : content.getFrom();

        message.setSubject(content.getSubject());
        message.setFrom(from);
        message.setTo(content.getTo());

        if (StringUtils.hasText(content.getFrom())) {
            message.setReplyTo(content.getReplyTo());
        }

        if (StringUtils.hasText(content.getCc())) {
            message.setCc(content.getCc());
        }

        if (StringUtils.hasText(content.getBcc())) {
            message.setBcc(content.getBcc());
        }

        message.setText(content.getText());

        javaMailSender.send(message);
    }
}
