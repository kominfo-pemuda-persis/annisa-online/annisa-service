package org.pemudipersis.annisaservice.service.mail;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.pemudipersis.annisaservice.util.StringUtil;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.48
 */
@AllArgsConstructor
@Value(staticConstructor = "valueOf")
public class MailMessageContent {
    @NotBlank
    String subject;

    @Email
    String from;

    @Email
    String replyTo;

    @NotBlank
    @Email
    String to;

    @Email
    String cc;

    @Email
    String bcc;

    String text;

    public MailMessageContent() {
        subject = "Default email subject";
        from = StringUtil.convertToInternetAddress("pp.pemuda.persis@gmail.com");
        replyTo = null;
        to = StringUtil.convertToInternetAddress("pp.pemuda.persis@gmail.com");
        cc = null;
        bcc = null;
        text = "Sample text of MailMessageContent";
    }

    public MailMessageContent(@NotBlank String subject,
                              @Email String from,
                              @NotBlank @Email String to, String text) {
        this.subject = subject;
        this.from = StringUtil.convertToInternetAddress(from);
        this.to = StringUtil.convertToInternetAddress(to);
        this.text = text;
        replyTo = null;
        cc = null;
        bcc = null;
    }

    public MailMessageContent(@NotBlank String subject,
                              @NotBlank @Email String to, String text) {
        this.subject = subject;
        this.to = StringUtil.convertToInternetAddress(to);
        this.text = text;
        from = null;
        replyTo = null;
        cc = null;
        bcc = null;
    }
}

