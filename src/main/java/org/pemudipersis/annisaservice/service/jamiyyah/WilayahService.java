package org.pemudipersis.annisaservice.service.jamiyyah;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;
import org.pemudipersis.annisaservice.entity.jamiyyah.Wilayah;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.jamiyyah.WilayahRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.17
 */
@Service
@Log4j2
public class WilayahService {
    private final WilayahRepository wilayahRepository;
    private final ModelMapper modelMapper;

    public WilayahService(WilayahRepository wilayahRepository, ModelMapper modelMapper) {
        this.wilayahRepository = wilayahRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<WilayahDto>> getAllWilayah(Pageable pageable, boolean isDeleted) {
        final long start = System.nanoTime();
        Pageable pageReq = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
        Page<WilayahDto> wilayahs = wilayahRepository.findAll(pageReq).map(this::convertToDto);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", wilayahs);
    }

    private WilayahDto convertToDto(Wilayah wilayah) {
        return modelMapper.map(wilayah, WilayahDto.class);
    }

    public BaseResponse<WilayahDto> updateWilayah(Wilayah wilayah, String id) {
        final long start = System.nanoTime();

        Optional<Wilayah> optionalWilayah = wilayahRepository.findByKdPw(id);
        if (!optionalWilayah.isPresent()) {
            throw new BusinessException(HttpStatus.NOT_FOUND, "30121", "Wilayah Not Found");
        }
        Wilayah currentWilayah = optionalWilayah.get();
        currentWilayah.setUpdatedAt(LocalDateTime.now());
        currentWilayah.setDiresmikan(wilayah.getDiresmikan());
        currentWilayah.setNamaWilayah(wilayah.getNamaWilayah());
        Wilayah result = wilayahRepository.save(currentWilayah);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<WilayahDto> create(Wilayah wilayah) {
        final long start = System.nanoTime();

        Optional<Wilayah> optionalWilayah = wilayahRepository.findByKdPw(wilayah.getKdPw());

        if (optionalWilayah.isPresent()) {
            throw new BusinessException(HttpStatus.FOUND, "KD_PW_ALREADY_EXIST", "KD_PW_ALREADY_EXIST");
        }

        Wilayah wilayahEntity = Wilayah.builder()
                .kdPw(wilayah.getKdPw())
                .namaWilayah(wilayah.getNamaWilayah())
                .createdAt(LocalDateTime.now())
                .diresmikan(wilayah.getDiresmikan())
                .updatedAt(LocalDateTime.now())
                //.isDeleted(false)
                .build();
        Wilayah result = wilayahRepository.save(wilayahEntity);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));

    }

    public BaseResponse<WilayahDto> delete(Principal principal, String id) {
        final long start = System.nanoTime();
        Wilayah wilayah = wilayahRepository.findByKdPw(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KD_PW_NOT_FOUND", "KD PW NOT FOUND"));
        //wilayah.setIsDeleted(true);
        wilayah.setUpdatedAt(LocalDateTime.now());
        wilayahRepository.save(wilayah);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public Wilayah getByKodeWilayah(String kodeWilayah) {
        Optional<Wilayah> optionalWilayah = wilayahRepository.findByKdPw(kodeWilayah);
        if (optionalWilayah.isPresent())
            return optionalWilayah.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "KD_PW_NOT_FOUND", "KD PW NOT FOUND");
    }
}
