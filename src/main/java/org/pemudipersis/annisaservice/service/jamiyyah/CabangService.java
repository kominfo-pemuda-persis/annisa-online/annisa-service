package org.pemudipersis.annisaservice.service.jamiyyah;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.entity.jamiyyah.Cabang;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.jamiyyah.CabangRepository;
import org.pemudipersis.annisaservice.request.CabangRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.20
 */
@Service
@Log4j2
public class CabangService {
    private final CabangRepository cabangRepository;
    private final ModelMapper modelMapper;

    public CabangService(CabangRepository cabangRepository, ModelMapper modelMapper) {
        this.cabangRepository = cabangRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<CabangRequest>> getList(Pageable pageable) {
        final long start = System.nanoTime();

        Page<CabangRequest> cabangs = cabangRepository.findAll(pageable).map(this::convertToDto);
        log.info("Retrieving Data Cabang ...");

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", cabangs);
    }

    public BaseResponse<CabangRequest> create(CabangRequest request) {
        final long start = System.nanoTime();

        Cabang cabang = Cabang.builder()
                .kdPc(request.getKdPc())
                .namaPc(request.getNamaPc())
                .createdAt(new Timestamp(System.currentTimeMillis()))
                //.isDeleted(false)
                .build();
        Cabang result = cabangRepository.save(cabang);
        log.info("Create new cabang (PC) ...");

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<CabangRequest> update(String kd, CabangRequest request) {
        final long start = System.nanoTime();

        Cabang cabang = cabangRepository.findBykdPc(kd)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KODE_CABANG_NOT_FOUND", "Kode Cabang " +
                        "not found"));
        cabang.setKdPc(request.getKdPc());
        cabang.setNamaPc(request.getNamaPc());
        cabang.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        Cabang result = cabangRepository.save(cabang);
        log.info("Update {} cabang kd " + kd);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private CabangRequest convertToDto(Cabang cabang) {
        return modelMapper.map(cabang, CabangRequest.class);
    }

    public BaseResponse<CabangRequest> delete(String kd) {
        final long start = System.nanoTime();

        Cabang cabang = cabangRepository.findBykdPc(kd)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KODE_CABANG_NOT_FOUND", "Kode Cabang " +
                        "not found"));
        //cabang.setIsDeleted(true);
        cabangRepository.save(cabang);
        log.info("Deleted {} cabang kd " + kd);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public Cabang getByKodeCabang(String kodeCabang) {
        Optional<Cabang> cabangOptional = cabangRepository.findBykdPc(kodeCabang);
        if (cabangOptional.isPresent())
            return cabangOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "KODE_CABANG_NOT_FOUND", "Kode Cabang not found");
    }
}

