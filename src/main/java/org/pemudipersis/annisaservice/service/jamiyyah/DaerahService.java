package org.pemudipersis.annisaservice.service.jamiyyah;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.jamiyyah.DaerahRepository;
import org.pemudipersis.annisaservice.request.DaerahRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.22
 */
@Service
@Log4j2
public class DaerahService {

    private final DaerahRepository daerahRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public DaerahService(DaerahRepository daerahRepository, ModelMapper modelMapper) {
        this.daerahRepository = daerahRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<DaerahRequest>> getList(Pageable pageable) {
        final long start = System.nanoTime();

        Page<DaerahRequest> daerahPage = daerahRepository.findAll(pageable).map(this::convertToDto);
        log.info("Fetching data Daerah from database");

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", daerahPage);

    }

    private DaerahRequest convertToDto(Daerah daerah) {
        return modelMapper.map(daerah, DaerahRequest.class);
    }

    @Transactional
    public BaseResponse<DaerahRequest> saveOne(DaerahRequest daerahRequest) {
        final long start = System.nanoTime();

        Daerah daerah = Daerah.builder()
                .kdPd(daerahRequest.getKdPd())
                .namaPd(daerahRequest.getNamaPd())
                //.isDeleted(false)
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        Daerah result = daerahRepository.save(daerah);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));

    }

    @Transactional
    public BaseResponse<DaerahRequest> updateOne(String id, DaerahRequest daerahRequest) {
        final long start = System.nanoTime();

        Daerah daerahFounded = daerahRepository.findBykdPd(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KODE_DAERAH_NOT_FOUND", "Kode Daerah " +
                        "not found"));
        daerahFounded.setKdPd(daerahRequest.getKdPd());
        daerahFounded.setNamaPd(daerahRequest.getNamaPd());
        daerahFounded.setDiresmikan(daerahRequest.getDiresmikan());
        daerahFounded.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        Daerah result = daerahRepository.save(daerahFounded);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));

    }

    @Transactional
    public BaseResponse<DaerahRequest> softDelete(String id) {
        final long start = System.nanoTime();
        Daerah daerahFounded = daerahRepository.findBykdPd(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KODE_DAERAH_NOT_FOUND", "Kode Daerah " +
                        "not found"));
        daerahFounded.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        daerahRepository.save(daerahFounded);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);

    }

    public Daerah getByKodeDaerah(String kodeDaerah) {
        Optional<Daerah> optionalDaerah = daerahRepository.findBykdPd(kodeDaerah);
        if (optionalDaerah.isPresent())
            return optionalDaerah.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "KODE_DAERAH_NOT_FOUND", "Kode Daerah not found");
    }
}
