package org.pemudipersis.annisaservice.service.anggota;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.anggota.PendidikanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.MasterPendidikanDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.MasterPendidikan;
import org.pemudipersis.annisaservice.entity.anggota.Pendidikan;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.PendidikanRepository;
import org.pemudipersis.annisaservice.repository.jamiyyah.MasterPendidikanRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.08
 */
@Service
@Log4j2
public class PendidikanService {
    public static final String SUCCESS = "SUCCESS";
    private final PendidikanRepository pendidikanRepository;

    private final AnggotaService anggotaService;

    private final MasterPendidikanRepository masterPendidikanRepository;

    private final ModelMapper modelMapper;

    public PendidikanService(PendidikanRepository pendidikanRepository, AnggotaService anggotaService,
                             MasterPendidikanRepository masterPendidikanRepository, ModelMapper modelMapper) {
        this.pendidikanRepository = pendidikanRepository;
        this.anggotaService = anggotaService;
        this.masterPendidikanRepository = masterPendidikanRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<PendidikanDto>> pendidikanList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();

        Page<PendidikanDto> pendidikanPage;
        if (!isDeleted)
            pendidikanPage =
                    pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpa(npa, pageable).map(this::convertToDto);
        else
            pendidikanPage =
                    pendidikanRepository.findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable).map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, pendidikanPage);

    }

    private PendidikanDto convertToDto(Pendidikan pendidikan) {
        return modelMapper.map(pendidikan, PendidikanDto.class);
    }

    public BaseResponse<PendidikanDto> create(String npa, PendidikanDto pendidikanDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);

        MasterPendidikan jenjangPendidikan =
                masterPendidikanRepository.findById(pendidikanDto.getJenjangPendidikan().getId()).
                orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "JENJANG_PENDIDIKAN_NOT_FOUND",
                        "Jenjang Pendidikan Not Found"));

        Pendidikan pendidikan = Pendidikan.builder()
                .anggota(anggota)
                .instansi(pendidikanDto.getInstansi())
                .jenisPendidikan(pendidikanDto.getJenisPendidikan())
                .jenjangPendidikan(jenjangPendidikan)
                .jurusan(pendidikanDto.getJurusan())
                .tahunKeluar(pendidikanDto.getTahunKeluar())
                .tahunMasuk(pendidikanDto.getTahunMasuk())
                .build();
        Pendidikan result = pendidikanRepository.save(pendidikan);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, convertToDto(result));

    }

    public BaseResponse<PendidikanDto> update(String npa, Integer idPendidikan, PendidikanDto pendidikanDto) {
        final long start = System.nanoTime();

        Pendidikan pendidikanExist = pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(npa,
                idPendidikan)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "PENDIDIKAN_NOT_FOUND", "Pendidikan " +
                        "Not Found"));

        int idJenjangPendidikan = Optional.ofNullable(pendidikanDto.getJenjangPendidikan())
                .map(MasterPendidikanDto::getId)
                .orElse(pendidikanExist.getJenjangPendidikan().getId());

        MasterPendidikan jenjangPendidikan = masterPendidikanRepository.findById(idJenjangPendidikan).
                orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "JENJANG_PENDIDIKAN_NOT_FOUND",
                        "Jenjang Pendidikan Not Found"));

        pendidikanDto.setJenjangPendidikan(null);
        pendidikanDto.setIdPendidikan(null);
        updateEntityFromDto(pendidikanDto, pendidikanExist);
        pendidikanExist.setJenjangPendidikan(jenjangPendidikan);

        Pendidikan result = pendidikanRepository.save(pendidikanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, convertToDto(result));

    }

    public BaseResponse<PendidikanDto> delete(String npa, Integer idPendidikan) {
        final long start = System.nanoTime();

        Pendidikan pendidikanExist = pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(npa,
                idPendidikan)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "PENDIDIKAN_NOT_FOUND", "Pendidikan " +
                        "Not Found"));

        pendidikanExist.setDeletedAt(LocalDateTime.now());
        pendidikanRepository.save(pendidikanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, null);

    }

    public BaseResponse<PendidikanDto> restore(String npa, Integer idPendidikan) {
        final long start = System.nanoTime();

        Pendidikan pendidikanExist = pendidikanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(npa,
                idPendidikan)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "PENDIDIKAN_NOT_FOUND", "Pendidikan " +
                        "Not Found"));

        pendidikanExist.setDeletedAt(null);
        Pendidikan result = pendidikanRepository.save(pendidikanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, convertToDto(result));

    }

    private void updateEntityFromDto(PendidikanDto from, Pendidikan to) {
        modelMapper.map(from, to);
    }
}

