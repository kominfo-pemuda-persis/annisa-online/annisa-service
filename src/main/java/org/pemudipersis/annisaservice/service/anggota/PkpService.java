package org.pemudipersis.annisaservice.service.anggota;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.anggota.PkpDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.Pkp;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.PkpRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.09
 */
@Service
@Log4j2
public class PkpService {
    private final PkpRepository pkpRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public PkpService(PkpRepository pkpRepository, AnggotaService anggotaService, ModelMapper modelMapper) {
        this.pkpRepository = pkpRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<PkpDto>> pkpList(Pageable pageable, boolean isDeleted, String idAnggota) {
        final long start = System.nanoTime();

        Page<PkpDto> pkpPage;
        if (!isDeleted)
            pkpPage = pkpRepository.findByDeletedAtIsNullAndAnggotaNpa(idAnggota, pageable).map(this::convertToDto);
        else
            pkpPage =
                    pkpRepository.findByDeletedAtIsNotNullAndAnggotaNpa(idAnggota, pageable).map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", pkpPage);
    }

    private PkpDto convertToDto(Pkp pkp) {
        return modelMapper.map(pkp, PkpDto.class);
    }

    public BaseResponse<PkpDto> create(String idAnggota, PkpDto PkpDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(idAnggota);
        Pkp pkp = Pkp.builder()
                .anggota(anggota)
                .lokasi(PkpDto.getLokasi())
                .tanggalMasuk(PkpDto.getTanggalMasuk())
                .tanggalSelesai(PkpDto.getTanggalSelesai())
                .build();
        Pkp result = pkpRepository.save(pkp);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<PkpDto> update(String idAnggota, Long idPkp, PkpDto pkpDto) {
        final long start = System.nanoTime();

        Pkp pkpExist = pkpRepository.findByDeletedAtIsNullAndAnggotaNpaAndId(idAnggota, idPkp)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "pkp_NOT_FOUND", "Pkp Not Found"));

        pkpDto.setIdPkp(null);
        updateEntityFromDto(pkpDto, pkpExist);

        Pkp result = pkpRepository.save(pkpExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<PkpDto> delete(String idAnggota, Long idpkp) {
        final long start = System.nanoTime();

        Pkp pkpExist = pkpRepository.findByDeletedAtIsNullAndAnggotaNpaAndId(idAnggota, idpkp)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "pkp_NOT_FOUND", "pkp Not Found"));

        pkpExist.setDeletedAt(LocalDateTime.now());
        pkpRepository.save(pkpExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<PkpDto> restore(String idAnggota, Long idPkp) {
        final long start = System.nanoTime();

        Pkp PkpExist = pkpRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndId(idAnggota, idPkp)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "Pkp_NOT_FOUND", "Pkp Not Found"));

        PkpExist.setDeletedAt(null);
        Pkp result = pkpRepository.save(PkpExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(PkpDto from, Pkp to) {
        modelMapper.map(from, to);
    }
}

