package org.pemudipersis.annisaservice.service.anggota;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.anggota.OrganisasiDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.Organisasi;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.OrganisasiRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.06
 */
@Service
@Log4j2
public class OrganisasiService {
    private final OrganisasiRepository organisasiRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public OrganisasiService(OrganisasiRepository organisasiRepository, AnggotaService anggotaService,
                             ModelMapper modelMapper) {
        this.organisasiRepository = organisasiRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<OrganisasiDto>> organisasiList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();

        Page<OrganisasiDto> organisasiPage;
        if (!isDeleted)
            organisasiPage =
                    organisasiRepository.findByDeletedAtIsNullAndAnggotaNpa(npa, pageable).map(this::convertToDto);
        else
            organisasiPage =
                    organisasiRepository.findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable).map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", organisasiPage);

    }

    private OrganisasiDto convertToDto(Organisasi organisasi) {
        return modelMapper.map(organisasi, OrganisasiDto.class);
    }

    public BaseResponse<OrganisasiDto> create(String npa, OrganisasiDto organisasiDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);
        Organisasi organisasi = Organisasi.builder()
                .anggota(anggota)
                .jabatan(organisasiDto.getJabatan())
                .lokasi(organisasiDto.getLokasi())
                .namaOrganisasi(organisasiDto.getNamaOrganisasi())
                .tahunMulai(organisasiDto.getTahunMulai())
                .tahunSelesai(organisasiDto.getTahunSelesai())
                .build();
        Organisasi result = organisasiRepository.save(organisasi);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<OrganisasiDto> update(String npa, Integer idOrganisasi, OrganisasiDto organisasiDto) {
        final long start = System.nanoTime();

        Organisasi organisasiExist = organisasiRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(npa,
                idOrganisasi)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "ORGANISASI_NOT_FOUND", "Organisasi " +
                        "Not Found"));
        organisasiDto.setIdOrganisasi(null);
        updateEntityFromDto(organisasiDto, organisasiExist);

        Organisasi result = organisasiRepository.save(organisasiExist);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<OrganisasiDto> delete(String idAnggota, Integer idOrganisasi) {
        final long start = System.nanoTime();

        Organisasi organisasiExist = organisasiRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(idAnggota
                , idOrganisasi)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "ORGANISASI_NOT_FOUND", "Organisasi " +
                        "Not Found"));

        organisasiExist.setDeletedAt(LocalDateTime.now());
        Organisasi result = organisasiRepository.save(organisasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<OrganisasiDto> restore(String npa, Integer idOrganisasi) {
        final long start = System.nanoTime();

        Organisasi organisasiExist = organisasiRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(npa,
                idOrganisasi)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "ORGANISASI_NOT_FOUND", "Organisasi " +
                        "Not Found"));

        organisasiExist.setDeletedAt(null);
        Organisasi result = organisasiRepository.save(organisasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(OrganisasiDto from, Organisasi to) {
        modelMapper.map(from, to);
    }
}


