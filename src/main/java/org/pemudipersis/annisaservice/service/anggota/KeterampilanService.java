package org.pemudipersis.annisaservice.service.anggota;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.anggota.KeterampilanDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.Keterampilan;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.KeterampilanRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.05
 */
@Service
@Log4j2
public class KeterampilanService {
    private final KeterampilanRepository keterampilanRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public KeterampilanService(KeterampilanRepository keterampilanRepository, AnggotaService anggotaService,
                               ModelMapper modelMapper) {
        this.keterampilanRepository = keterampilanRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<KeterampilanDto>> keterampilanList(Pageable pageable, boolean isDeleted,
                                                                String idAnggota) {
        final Long start = System.nanoTime();

        Page<KeterampilanDto> keterampilanPage;
        if (!isDeleted)
            keterampilanPage =
                    keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpa(idAnggota, pageable).map(this::convertToDto);
        else
            keterampilanPage =
                    keterampilanRepository.findByDeletedAtIsNotNullAndAnggotaNpa(idAnggota, pageable).map(this::convertToDto);

        final Long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", keterampilanPage);
    }

    public BaseResponse<KeterampilanDto> create(String idAnggota, KeterampilanDto keterampilanDto) {
        final Long start = System.nanoTime();
        Anggota anggota = anggotaService.findByNpa(idAnggota);

        Keterampilan keterampilan = Keterampilan.builder()
                .anggota(anggota)
                .keterampilan(keterampilanDto.getKeterampilan())
                .build();

        Keterampilan result = keterampilanRepository.save(keterampilan);

        final Long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<KeterampilanDto> update(String idAnggota, Integer idKeterampilan,
                                                KeterampilanDto keterampilanDto) {
        final Long start = System.nanoTime();
        Keterampilan keterampilanExist =
                keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(idAnggota, idKeterampilan)
                        .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KETERAMPILAN_NOT_FOUND",
                                "Keterampilan Not Found"));

        keterampilanDto.setIdKeterampilan(null);
        updateEntityFromDto(keterampilanDto, keterampilanExist);

        Keterampilan result = keterampilanRepository.save(keterampilanExist);

        final Long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<KeterampilanDto> delete(String idAnggota, Integer idKeterampilan) {
        final Long start = System.nanoTime();
        Keterampilan keterampilanExist =
                keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(idAnggota, idKeterampilan)
                        .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KETERAMPILAN_NOT_FOUND",
                                "Keterampilan Not Found"));
        keterampilanExist.setDeletedAt(LocalDateTime.now());

        Keterampilan result = keterampilanRepository.save(keterampilanExist);

        final Long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<KeterampilanDto> restore(String idAnggota, Integer idKeterampilan) {
        final Long start = System.nanoTime();
        Keterampilan keterampilanExist =
                keterampilanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(idAnggota, idKeterampilan)
                        .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KETERAMPILAN_NOT_FOUND",
                                "Keterampilan Not Found"));
        keterampilanExist.setDeletedAt(null);

        Keterampilan result = keterampilanRepository.save(keterampilanExist);

        final Long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(KeterampilanDto from, Keterampilan to) {
        modelMapper.map(from, to);
    }

    private KeterampilanDto convertToDto(Keterampilan keterampilan) {
        return modelMapper.map(keterampilan, KeterampilanDto.class);
    }
}

