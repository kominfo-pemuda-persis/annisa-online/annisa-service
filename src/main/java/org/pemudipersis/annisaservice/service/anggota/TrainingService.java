package org.pemudipersis.annisaservice.service.anggota;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.anggota.TrainingDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.Training;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.TrainingRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.12
 */
@Service
@Log4j2
public class TrainingService {
    private final TrainingRepository trainingRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public TrainingService(TrainingRepository trainingRepository, AnggotaService anggotaService,
                           ModelMapper modelMapper) {
        this.trainingRepository = trainingRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<TrainingDto>> trainingList(Pageable pageable, boolean isDeleted, String idAnggota) {
        final long start = System.nanoTime();

        Page<TrainingDto> trainingPage;
        if (!isDeleted)
            trainingPage =
                    trainingRepository.findByDeletedAtIsNullAndAnggotaNpa(idAnggota, pageable).map(this::convertToDto);
        else
            trainingPage =
                    trainingRepository.findByDeletedAtIsNotNullAndAnggotaNpa(idAnggota, pageable).map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", trainingPage);
    }

    private TrainingDto convertToDto(Training training) {
        return modelMapper.map(training, TrainingDto.class);
    }

    public BaseResponse<TrainingDto> create(String idAnggota, TrainingDto trainingDto) {
        final long start = System.nanoTime();
        Anggota anggota = anggotaService.findByNpa(idAnggota);

        Training training = Training.builder()
                .anggota(anggota)
                .jenis(trainingDto.getJenis())
                .namaTraining(trainingDto.getNamaTraining())
                .penyelenggara(trainingDto.getPenyelenggara())
                .tanggalMulai(trainingDto.getTanggalMulai())
                .tanggalSelesai(trainingDto.getTanggalSelesai())
                .tempat(trainingDto.getTempat())
                .build();
        Training result = trainingRepository.save(training);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<TrainingDto> update(String idAnggota, Long idTraining, TrainingDto trainingDto) {
        final long start = System.nanoTime();

        Training trainingExist = trainingRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(idAnggota,
                idTraining)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "TRAINING_NOT_FOUND", "Training Not " +
                        "Found"));
        trainingDto.setIdTraining(null);
        updateEntityFromDto(trainingDto, trainingExist);
        Training result = trainingRepository.save(trainingExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<TrainingDto> delete(String idAnggota, Long idTraining) {
        final long start = System.nanoTime();

        Training trainingExist = trainingRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(idAnggota,
                idTraining)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "TRAINING_NOT_FOUND", "Training Not " +
                        "Found"));
        trainingExist.setDeletedAt(LocalDateTime.now());
        trainingRepository.save(trainingExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<TrainingDto> restore(String idAnggota, Long idTraining) {
        final long start = System.nanoTime();

        Training trainingExist = trainingRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(idAnggota,
                idTraining)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "TRAINING_NOT_FOUND", "Training Not " +
                        "Found"));
        trainingExist.setDeletedAt(null);
        Training result = trainingRepository.save(trainingExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(TrainingDto from, Training to) {
        modelMapper.map(from, to);
    }
}

