package org.pemudipersis.annisaservice.service.anggota;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.constant.GlobalMessage;
import org.pemudipersis.annisaservice.dto.anggota.AnggotaDto;
import org.pemudipersis.annisaservice.dto.anggota.AnggotaRequestDto;
import org.pemudipersis.annisaservice.dto.anggota.BaseImageDto;
import org.pemudipersis.annisaservice.dto.anggota.CheckAnggotaDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.DesaDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KabupatenDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KecamatanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.MasterPendidikanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.ProvinsiDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.Login;
import org.pemudipersis.annisaservice.entity.anggota.MasterPendidikan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Cabang;
import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.pemudipersis.annisaservice.entity.jamiyyah.Desa;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kecamatan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Provinsi;
import org.pemudipersis.annisaservice.entity.jamiyyah.Wilayah;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResourceNotFoundException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.AnggotaRepository;
import org.pemudipersis.annisaservice.request.CabangRequest;
import org.pemudipersis.annisaservice.request.CheckAnggotaRequest;
import org.pemudipersis.annisaservice.request.DaerahRequest;
import org.pemudipersis.annisaservice.request.SendEmailAnggotaRequest;
import org.pemudipersis.annisaservice.service.HelperUtilService;
import org.pemudipersis.annisaservice.service.jamiyyah.CabangService;
import org.pemudipersis.annisaservice.service.jamiyyah.DaerahService;
import org.pemudipersis.annisaservice.service.jamiyyah.WilayahService;
import org.pemudipersis.annisaservice.service.mail.MailMessageContent;
import org.pemudipersis.annisaservice.service.mail.MailService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.33
 */
@Service
@Log4j2
@Transactional
@RequiredArgsConstructor
public class AnggotaService {
    public static final String DATA = "data";
    public static final String FILE_NAME = "fileName";
    private static final String BASE_PREFIX_IMAGE_ANGGOTA = "images/anggota/";
    private static final String SLASH_STRING = "/";
    private static final String EMPTY_STRING = "";
    private final AnggotaRepository anggotaRepository;
    private final ModelMapper modelMapper;
    private final HelperUtilService helperUtilService;
    private final WilayahService wilayahService;
    private final CabangService cabangService;
    private final DaerahService daerahService;
    private final LoginService loginService;
    private final MailService mailService;
    private final StorageService storageService;

    //@PreAuthorize("hasAuthority('read-anggota')")
    public BaseResponse<Page<AnggotaDto>> getAllAnggota(Pageable pageable) {
        final long start = System.nanoTime();
        Page<AnggotaDto> anggotas = anggotaRepository.findAllByDeletedAtIsNull(pageable)
                .map(this::convertToDto);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                anggotas);

    }

    //@PreAuthorize("hasAuthority('read-anggota')")
    public BaseResponse<AnggotaDto> getByNpa(String npa) {
        final long start = System.nanoTime();
        Anggota result = anggotaRepository.findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "User Not Found"));
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                convertToDto(result));
    }

    public Anggota findByNpa(String npa) {
        return anggotaRepository.findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
    }

    public Anggota getById(String idAnggota) {
        Optional<Anggota> optionalAnggota = anggotaRepository.findById(idAnggota);
        if (!optionalAnggota.isPresent()) {
            throw new BusinessException(HttpStatus.NOT_FOUND, GlobalMessage.ANGGOTA_NOT_FOUND.code,
                    GlobalMessage.ANGGOTA_NOT_FOUND.message);
        }
        return optionalAnggota.get();
    }

    //@PreAuthorize("hasAuthority('read-anggota')")
    public BaseResponse<AnggotaDto> updateCurrentAnggota(Principal principal, String npa,
                                                         AnggotaRequestDto anggotaDto) {
        final long start = System.nanoTime();

        if (!principal.getName().equalsIgnoreCase(anggotaDto.getNpa())) {
            throw new BusinessException(HttpStatus.FORBIDDEN, "access_denied", "Access is denied");
        }

        Anggota anggotaExist = anggotaRepository.findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "User Not Found"));

        this.setReferenceUpdateData(anggotaDto, anggotaExist);
        anggotaExist.setNpa(npa);
        anggotaExist.setLastUpdated(LocalDateTime.now());
        Anggota result = anggotaRepository.save(anggotaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                convertToDto(result));
    }

    public BaseResponse<AnggotaDto> create(Principal principal, AnggotaRequestDto anggotaDto) {
        final long start = System.nanoTime();

        Login user = loginService.getUserByNpa(principal.getName());
        Anggota anggota = convertToEntity(anggotaDto);
        this.setReferenceData(anggota, anggotaDto);
        anggota.setFoto("default.png");
        anggota.setCreatedDate(LocalDateTime.now());
        anggota.setCreatedBy(user.getIdLogin());
        anggota.setLastUpdated(LocalDateTime.now());
        Anggota result = anggotaRepository.save(anggota);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                convertToDto(result));
    }

    private AnggotaDto convertToDto(Anggota anggota) {
        return modelMapper.map(anggota, AnggotaDto.class);
    }


    private void setReferenceData(Anggota anggota, AnggotaRequestDto dto) {
        if (anggotaRepository.findByNpa(dto.getNpa()).isPresent()) {
            throw new BusinessException(HttpStatus.FOUND, "NPA_ALREADY_EXIST", "NPA ALREADY EXIST");
        }
        Provinsi provinsi = helperUtilService.getProvinsiById(dto.getProvinsi().getId());
        Kabupaten kabupaten = helperUtilService.getKabupatenById(dto.getKabupaten().getId());
        Kecamatan kecamatan = helperUtilService.getKecamatanById(dto.getKecamatan().getId());
        MasterPendidikan masterPendidikan =
                helperUtilService.getMasterPendidikanById(dto.getJenjangPendidikan().getId());
        Desa desa = helperUtilService.getDesaByid(dto.getDesa().getId());
        Wilayah pw = wilayahService.getByKodeWilayah(dto.getPw().getKdPw());
        Cabang pc = cabangService.getByKodeCabang(dto.getPc().getKdPc());
        Daerah pd = daerahService.getByKodeDaerah(dto.getPd().getKdPd());
        anggota.setKecamatan(kecamatan);
        anggota.setJenjangPendidikan(masterPendidikan);
        anggota.setProvinsi(provinsi);
        anggota.setDesa(desa);
        anggota.setPc(pc);
        anggota.setPw(pw);
        anggota.setPd(pd);
        anggota.setKabupaten(kabupaten);
    }


    private Anggota convertToEntity(AnggotaRequestDto anggotaDto) {
        return modelMapper.map(anggotaDto, Anggota.class);
    }

    public BaseResponse<AnggotaDto> update(Principal principal, String idAnggota, AnggotaRequestDto anggotaDto) {
        final long start = System.nanoTime();
        Anggota anggotaExist = anggotaRepository.findById(idAnggota)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
        this.setReferenceUpdateData(anggotaDto, anggotaExist);
        anggotaExist.setLastUpdated(LocalDateTime.now());
        Anggota result = anggotaRepository.save(anggotaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                convertToDto(result));
    }

    private void setReferenceUpdateData(AnggotaRequestDto anggotaDto, Anggota anggotaExist) {

        boolean npaIsNotUsed = anggotaRepository.findByNpa(anggotaDto.getNpa())
                .map(anggota -> anggota.getId().equals(anggotaExist.getId()))
                .orElse(true);

        if (!npaIsNotUsed) {
            throw new BusinessException(HttpStatus.FOUND, GlobalMessage.NPA_ALREADY_EXIST.code,
                    GlobalMessage.NPA_ALREADY_EXIST.message);
        }

        Provinsi provinsi = Optional.ofNullable(anggotaDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .map(helperUtilService::getProvinsiById)
                .orElse(anggotaExist.getProvinsi());
        anggotaExist.setProvinsi(provinsi);
        anggotaDto.setProvinsi(null);

        Kabupaten kabupaten = Optional.ofNullable(anggotaDto.getKabupaten())
                .map(KabupatenDto::getId)
                .map(helperUtilService::getKabupatenById)
                .orElse(anggotaExist.getKabupaten());
        anggotaExist.setKabupaten(kabupaten);
        anggotaDto.setKabupaten(null);

        Kecamatan kecamatan = Optional.ofNullable(anggotaDto.getKecamatan())
                .map(KecamatanDto::getId)
                .map(helperUtilService::getKecamatanById)
                .orElse(anggotaExist.getKecamatan());
        anggotaExist.setKecamatan(kecamatan);
        anggotaDto.setKecamatan(null);

        Wilayah wilayah = Optional.ofNullable(anggotaDto.getPw())
                .map(WilayahDto::getKdPw)
                .map(wilayahService::getByKodeWilayah)
                .orElse(anggotaExist.getPw());
        anggotaExist.setPw(wilayah);
        anggotaDto.setPw(null);

        Cabang cabang = Optional.ofNullable(anggotaDto.getPc())
                .map(CabangRequest::getKdPc)
                .map(cabangService::getByKodeCabang)
                .orElse(anggotaExist.getPc());
        anggotaExist.setPc(cabang);
        anggotaDto.setPc(null);

        Daerah daerah = Optional.ofNullable(anggotaDto.getPd())
                .map(DaerahRequest::getKdPd)
                .map(daerahService::getByKodeDaerah)
                .orElse(anggotaExist.getPd());
        anggotaExist.setPd(daerah);
        anggotaDto.setPd(null);

        Desa desa = Optional.ofNullable(anggotaDto.getDesa())
                .map(DesaDto::getId)
                .map(helperUtilService::getDesaByid)
                .orElse(anggotaExist.getDesa());
        anggotaExist.setDesa(desa);
        anggotaDto.setDesa(null);

        MasterPendidikan masterPendidikan = Optional.ofNullable(anggotaDto.getJenjangPendidikan())
                .map(MasterPendidikanDto::getId)
                .map(helperUtilService::getMasterPendidikanById)
                .orElse(anggotaExist.getJenjangPendidikan());
        anggotaExist.setJenjangPendidikan(masterPendidikan);
        anggotaDto.setJenjangPendidikan(null);

        modelMapper.map(anggotaDto, anggotaExist);

    }

    public BaseResponse<CheckAnggotaDto> check(CheckAnggotaRequest request) {
        return anggotaRepository.findByNpa(request.getNpa())
                .map(anggota -> ResponseBuilder.<BaseResponse<CheckAnggotaDto>>buildResponse(
                        HttpStatus.OK,
                        0,
                        "",
                        CheckAnggotaDto.valueOf(anggota)
                        )
                )
                .orElseGet(() -> ResponseBuilder.buildResponse(
                        HttpStatus.NOT_FOUND,
                        0,
                        "Data anggota tidak ditemukan",
                        null
                ));
    }

    public void sendEmail(SendEmailAnggotaRequest request) {
        Anggota anggota = anggotaRepository.findByNpa(request.getNpa())
                .orElseThrow(() -> new ResourceNotFoundException("Data anggota tidak ditemukan"));

        mailService.sendMail(new MailMessageContent(
                "Selamat datang di ANNISA Online",
                request.getEmail(),
                "Berikut informasi keanggotaan anda: " + anggota)
        );
    }

    public Long countAnggota() {
        return anggotaRepository.countByDeletedAtIsNull();
    }

    public List<Anggota> findAllByDeletedAtIsNull() {
        return anggotaRepository.findAllByDeletedAtIsNull();
    }

    public BaseResponse<String> uploadFotoAngotaByNpa(Principal principal, String npa, MultipartFile file) {
        final long start = System.nanoTime();
        Anggota anggota = setReferenceAndValidateData(npa, file);
        anggotaRepository.save(anggota);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                "successfully changed foto anggota : " + anggota.getNama());
    }

    private Anggota setReferenceAndValidateData(String npa, MultipartFile file) {
        Anggota anggota =
                anggotaRepository.findByNpa(npa).orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND,
                        GlobalMessage.ANGGOTA_NOT_FOUND.code, GlobalMessage.ANGGOTA_NOT_FOUND.message));
        String fileName = anggota.getNama().concat("_")
                .concat(LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy_HHmmss")))
                .concat(".")
                .concat(Objects.requireNonNull(StringUtils.getFilenameExtension(file.getOriginalFilename())));
        String prefixFileName = BASE_PREFIX_IMAGE_ANGGOTA
                .concat(anggota.getPw().getNamaWilayah().trim()).concat(SLASH_STRING)
                .concat(anggota.getPd().getNamaPd().trim()).concat(SLASH_STRING)
                .concat(anggota.getPc().getNamaPc().trim()).concat(SLASH_STRING)
                .concat(anggota.getNamaPj()).concat(SLASH_STRING)
                .concat(fileName);
        storageService.uploadFile(file, prefixFileName);
        anggota.setFoto(fileName);
        return anggota;
    }

    public Map<String, Object> downloadFotoAngotaByNpa(String npa, Principal principal) {
        return getDownloadFotoMap(npa);
    }

    public BaseResponse<BaseImageDto> detailFotoAnggotaByNpa(Principal principal, String npa) {
        return getDetailFoto(npa);
    }

    public BaseResponse<String> uploadFotoProfile(Principal principal, MultipartFile file) {
        final long start = System.nanoTime();
        Anggota anggota = setReferenceAndValidateData(principal.getName(), file);
        anggotaRepository.save(anggota);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                "Successfully changed your profile picture");
    }

    public BaseResponse<BaseImageDto> detailFotoProfile(Principal principal) {
        String npa = principal.getName();
        return getDetailFoto(npa);
    }

    private BaseResponse<BaseImageDto> getDetailFoto(String npa) {
        final long start = System.nanoTime();
        Anggota anggota = anggotaRepository.findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, GlobalMessage.NPA_NOT_FOUND.code,
                        GlobalMessage.NPA_NOT_FOUND.message));
        String prefixFotoLocated;
        if ("default.png".equals(anggota.getFoto())) {
            prefixFotoLocated = "static/".concat(anggota.getFoto());
        } else {
            prefixFotoLocated = BASE_PREFIX_IMAGE_ANGGOTA
                    .concat(anggota.getPw().getNamaWilayah().trim()).concat(SLASH_STRING)
                    .concat(anggota.getPd().getNamaPd().trim()).concat(SLASH_STRING)
                    .concat(anggota.getPc().getNamaPc().trim()).concat(SLASH_STRING)
                    .concat(anggota.getNamaPj()).concat(SLASH_STRING)
                    .concat(anggota.getFoto());
        }
        String imgScr = storageService.getUriEndPoint(prefixFotoLocated);

        BaseImageDto baseImageDto = BaseImageDto.builder()
                .imgScr(imgScr)
                .name(anggota.getNama().concat("(" + anggota.getNpa() + ")")).build();
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message,
                baseImageDto);
    }


    public Map<String, Object> downloadProfilePhoto(Principal principal) {
        return getDownloadFotoMap(principal.getName());
    }

    private Map<String, Object> getDownloadFotoMap(String npa) {
        Anggota anggota = anggotaRepository.findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
        String prefixFotoLocated;
        if ("default.png".equals(anggota.getFoto())) {
            prefixFotoLocated = "static/".concat(anggota.getFoto());
        } else {
            prefixFotoLocated = BASE_PREFIX_IMAGE_ANGGOTA
                    .concat(anggota.getPw().getNamaWilayah().trim()).concat(SLASH_STRING)
                    .concat(anggota.getPd().getNamaPd().trim()).concat(SLASH_STRING)
                    .concat(anggota.getPc().getNamaPc().trim()).concat(SLASH_STRING)
                    .concat(anggota.getNamaPj()).concat(SLASH_STRING)
                    .concat(anggota.getFoto());
        }

        byte[] imageByte = storageService.downloadFile(prefixFotoLocated);
        Map<String, Object> result = new LinkedHashMap<>();
        result.put(DATA, imageByte);
        result.put(FILE_NAME, anggota.getFoto());
        return result;
    }
}
