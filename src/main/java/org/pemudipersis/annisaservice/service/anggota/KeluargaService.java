package org.pemudipersis.annisaservice.service.anggota;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.pemudipersis.annisaservice.dto.anggota.KeluargaDto;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.pemudipersis.annisaservice.entity.anggota.Keluarga;
import org.pemudipersis.annisaservice.entity.jamiyyah.Otonom;
import org.pemudipersis.annisaservice.exception.BaseResponse;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.exception.ResponseBuilder;
import org.pemudipersis.annisaservice.repository.anggota.KeluargaRepository;
import org.pemudipersis.annisaservice.repository.jamiyyah.OtonomRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.04
 */
@Service
@Log4j2
public class KeluargaService {
    private final KeluargaRepository keluargaRepository;

    private final AnggotaService anggotaService;

    private final OtonomRepository otonomRepository;

    private final ModelMapper modelMapper;

    public KeluargaService(KeluargaRepository keluargaRepository, AnggotaService anggotaService,
                           OtonomRepository otonomRepository, ModelMapper modelMapper) {
        this.keluargaRepository = keluargaRepository;
        this.anggotaService = anggotaService;
        this.otonomRepository = otonomRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<KeluargaDto>> keluargaList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();
        Page<KeluargaDto> keluargaPage;
        if (!isDeleted)
            keluargaPage = keluargaRepository.findByDeletedAtIsNullAndAnggotaNpa(npa, pageable).map(this::convertToDto);
        else
            keluargaPage =
                    keluargaRepository.findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable).map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", keluargaPage);

    }

    private KeluargaDto convertToDto(Keluarga keluarga) {
        return modelMapper.map(keluarga, KeluargaDto.class);
    }

    public BaseResponse<KeluargaDto> create(String npa, KeluargaDto keluargaDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);
        Otonom otonom = otonomRepository.findById(keluargaDto.getOtonom().getIdOtonom())
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "OTONOM_NOT_FOUND", "Otonom Not Found"));

        Keluarga keluarga = Keluarga.builder()
                .anggota(anggota)
                .namaKeluarga(keluargaDto.getNamaKeluarga())
                .alamat(keluargaDto.getAlamat())
                .hubungan(keluargaDto.getHubungan())
                .jumlahAnak(keluargaDto.getJumlahAnak())
                .keterangan(keluargaDto.getKeterangan())
                .otonom(otonom)
                .statusAnggota(keluargaDto.getStatusAnggota())
                .build();
        Keluarga result = keluargaRepository.save(keluarga);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<KeluargaDto> update(String idAnggota, int idKeluarga, KeluargaDto keluargaDto) {
        final long start = System.nanoTime();

        Keluarga keluargaExist = keluargaRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(idAnggota,
                idKeluarga)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KELUARGA_NOT_FOUND", "Keluarga Not " +
                        "Found"));

        int idOtonom = Optional.ofNullable(keluargaDto.getOtonom())
                .map(Otonom::getIdOtonom)
                .orElse(keluargaExist.getOtonom().getIdOtonom());

        Otonom otonom = otonomRepository.findById(idOtonom)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "OTONOM_NOT_FOUND", "Otonom Not Found"));

        keluargaDto.setOtonom(null);
        keluargaDto.setIdKeluarga(null);
        updateEntityFromDto(keluargaDto, keluargaExist);
        keluargaExist.setOtonom(otonom);
        Keluarga result = keluargaRepository.save(keluargaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));

    }

    public BaseResponse<KeluargaDto> delete(String npa, int idKeluarga) {
        final long start = System.nanoTime();

        Keluarga keluargaExist = keluargaRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(npa, idKeluarga)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "30731", "Keluarga Not Found"));

        keluargaExist.setDeletedAt(LocalDateTime.now());
        keluargaRepository.save(keluargaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);

    }

    public BaseResponse<KeluargaDto> restore(String npa, int idKeluarga) {
        final long start = System.nanoTime();

        Keluarga keluargaExist = keluargaRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeluarga(npa, idKeluarga)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "KELUARGA_NOT_FOUND", "Keluarga Not " +
                        "Found"));

        keluargaExist.setDeletedAt(null);
        Keluarga result = keluargaRepository.save(keluargaExist);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(Object from, Object to) {
        modelMapper.map(from, to);
    }
}

