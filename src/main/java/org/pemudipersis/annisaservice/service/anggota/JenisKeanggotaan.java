package org.pemudipersis.annisaservice.service.anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 05.56
 */
public enum JenisKeanggotaan {
    BIASA,
    TERSIAR
}
