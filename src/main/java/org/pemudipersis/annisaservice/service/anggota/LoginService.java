package org.pemudipersis.annisaservice.service.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Login;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.pemudipersis.annisaservice.repository.user.LoginRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.57
 */
@Service
public class LoginService implements UserDetailsService {
    private final LoginRepository loginRepository;

    public LoginService(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String npa) {
        Optional<Login> optionalLogin = loginRepository.findByAnggotaNpa(npa);
        if (!optionalLogin.isPresent()) {
            throw new BusinessException(HttpStatus.CONFLICT, "30020", "Invalid NPA/Username or Password");
        }

        Login login = optionalLogin.get();
        return new org.springframework.security.core.userdetails.User(login.getAnggota().getNpa(),
                login.getPassword(), login.getAuthorities());
    }

    public Login getUserByNpa(String npa) {
        return loginRepository.findByAnggotaNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "USER NOT FOUND"));
    }

    public Long countUser() {
        return loginRepository.count();
    }

    private List getAuthority() {
        return List.of(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }
}
