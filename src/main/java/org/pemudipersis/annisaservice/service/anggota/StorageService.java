package org.pemudipersis.annisaservice.service.anggota;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.pemudipersis.annisaservice.exception.BusinessException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.43
 */
@Service
@Slf4j
public class StorageService {
    public static final String EMPTY_STRING = "";
    private static final List<String> contentTypes = Arrays.asList("image/png", "image/jpeg", "image/gif");
    private final AmazonS3 s3Client;
    @Value("${application.bucket.name}")
    private String bucketName;

    public StorageService(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public String uploadFile(MultipartFile file, String fileName) {
        validateFileType(file);
        File fileObj = convertMultiPartFileToFile(file);
        s3Client.putObject(new PutObjectRequest(bucketName, fileName, fileObj));
        fileObj.delete();
        return file.getOriginalFilename();
    }

    private void validateFileType(MultipartFile file) {
        String fileContentType = file.getContentType();
        if (!contentTypes.contains(fileContentType)) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "ONLY_IMAGE_TYPE_ARE_ALLOWED", "ONLY IMAGE TYPE ARE " +
                    "ALLOWED");
        }
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }

    public byte[] downloadFile(String fileName) {
        byte[] content = null;
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            content = IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public String moveFile(String source, String destination) {
        ObjectListing objectListing = s3Client.listObjects(new ListObjectsRequest()
                .withBucketName(bucketName)
                .withPrefix(EMPTY_STRING));

        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
            s3Client.copyObject(source, objectSummary.getKey(), destination, objectSummary.getKey());
        }
        return EMPTY_STRING;
    }

    public String deleteFile(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
        return fileName;
    }

    public String getUriEndPoint(String fileName) {
        return "http://" + bucketName + ".s3-ap-southeast-1.amazonaws.com/" + fileName;
    }
}
