package org.pemudipersis.annisaservice.repository.jamiyyah;

import org.pemudipersis.annisaservice.entity.jamiyyah.Desa;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.15
 */
public interface DesaRepository extends JpaRepository<Desa, String> {
}
