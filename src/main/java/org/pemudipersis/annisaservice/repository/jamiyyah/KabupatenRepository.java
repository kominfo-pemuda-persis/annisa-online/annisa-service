package org.pemudipersis.annisaservice.repository.jamiyyah;

import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.14
 */
public interface KabupatenRepository extends JpaRepository<Kabupaten, String> {
}
