package org.pemudipersis.annisaservice.repository.jamiyyah;

import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.22
 */
public interface DaerahRepository extends JpaRepository<Daerah, String> {
    Page<Daerah> findAll(Pageable pageable);

    Optional<Daerah> findBykdPd(String id);
}
