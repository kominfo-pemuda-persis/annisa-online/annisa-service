package org.pemudipersis.annisaservice.repository.user;

import org.pemudipersis.annisaservice.entity.anggota.Login;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 12.16
 */
public interface LoginRepository extends JpaRepository<Login, String> {
    Optional<Login> findByAnggotaNpa(String npa);
}