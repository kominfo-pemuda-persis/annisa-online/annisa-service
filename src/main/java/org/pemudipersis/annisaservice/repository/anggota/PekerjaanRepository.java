package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Pekerjaan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.58
 */
public interface PekerjaanRepository extends JpaRepository<Pekerjaan, Integer> {
    Page<Pekerjaan> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Pekerjaan> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Pekerjaan> findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(String idAnggota, Integer idPekerjaan);

    Optional<Pekerjaan> findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(String idAnggota, Integer idPekerjaan);
}

