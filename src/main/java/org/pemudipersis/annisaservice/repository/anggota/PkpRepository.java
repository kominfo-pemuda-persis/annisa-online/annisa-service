package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Pkp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.00
 */
public interface PkpRepository extends JpaRepository<Pkp, Long> {
    Page<Pkp> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Pkp> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Pkp> findByDeletedAtIsNotNullAndAnggotaNpaAndId(String idAnggota, Long idPkp);

    Optional<Pkp> findByDeletedAtIsNullAndAnggotaNpaAndId(String idAnggota, Long idPkp);
}
