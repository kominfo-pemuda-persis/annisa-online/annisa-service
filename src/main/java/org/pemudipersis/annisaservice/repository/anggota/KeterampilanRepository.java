package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Keterampilan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.52
 */
public interface KeterampilanRepository extends JpaRepository<Keterampilan, Integer> {
    Page<Keterampilan> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Keterampilan> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Keterampilan> findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(String idAnggota,
                                                                                  Integer idKeterampilan);

    Optional<Keterampilan> findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(String idAnggota,
                                                                               Integer idKeterampilan);
}
