package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Anggota;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.32
 */
public interface AnggotaRepository extends JpaRepository<Anggota, String> {

    Optional<Anggota> findByNpa(String npa);

    Page<Anggota> findAllByDeletedAtIsNull(Pageable pageable);

    List<Anggota> findAllByDeletedAtIsNull();

    Long countByDeletedAtIsNull();
}