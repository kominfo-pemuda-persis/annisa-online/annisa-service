package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Organisasi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.52
 */
public interface OrganisasiRepository extends JpaRepository<Organisasi, Integer> {
    Page<Organisasi> findByDeletedAtIsNullAndAnggotaNpa(String npa, Pageable pageable);

    Page<Organisasi> findByDeletedAtIsNotNullAndAnggotaNpa(String npa, Pageable pageable);

    Optional<Organisasi> findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(String npa, Integer idOrganisasi);

    Optional<Organisasi> findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(String npa, Integer idOrganisasi);
}

