package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Training;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 22.01
 */
public interface TrainingRepository extends JpaRepository<Training, Long> {
    Page<Training> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Training> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Training> findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(String idAnggota, Long idTraining);

    Optional<Training> findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(String idAnggota, Long idTraining);
}
