package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Keluarga;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.51
 */
public interface KeluargaRepository extends JpaRepository<Keluarga, Integer> {
    Page<Keluarga> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Keluarga> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Keluarga> findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeluarga(String idAnggota, Integer idKeluarga);

    Optional<Keluarga> findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(String idAnggota, Integer idKeluarga);
}
