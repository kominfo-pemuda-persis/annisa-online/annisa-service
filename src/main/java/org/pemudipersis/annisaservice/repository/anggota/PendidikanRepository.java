package org.pemudipersis.annisaservice.repository.anggota;

import org.pemudipersis.annisaservice.entity.anggota.Pendidikan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.59
 */
@Repository
public interface PendidikanRepository extends JpaRepository<Pendidikan, Integer> {
    Page<Pendidikan> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Pendidikan> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Pendidikan> findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(String idAnggota, Integer idPendidikan);

    Optional<Pendidikan> findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(String idAnggota, Integer idPendidikan);
}

