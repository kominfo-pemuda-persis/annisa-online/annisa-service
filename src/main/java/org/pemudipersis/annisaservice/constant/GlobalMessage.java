package org.pemudipersis.annisaservice.constant;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.26
 */
public enum GlobalMessage {
    SUCCESS("SUCCESS", "SUCCESS"),
    ERROR("ERROR", "ERROR"),
    ANGGOTA_NOT_FOUND("ANGGOTA_NOT_FOUND", "ANGGOTA NOT FOUND"),
    NPA_ALREADY_EXIST("NPA_ALREADY_EXIST", "NPA ALREADY EXIST"),
    NPA_NOT_FOUND("NPA_NOT_FOUND", "NPA NOT FOUND");

    public String code;
    public String message;

    GlobalMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
