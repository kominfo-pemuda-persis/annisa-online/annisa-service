package org.pemudipersis.annisaservice.response;

import jakarta.annotation.Nullable;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.16
 */
@Nullable
public class EmptyResponse implements Serializable {
    public EmptyResponse() {
    }
}
