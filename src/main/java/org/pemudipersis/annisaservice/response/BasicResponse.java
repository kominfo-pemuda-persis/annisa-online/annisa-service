package org.pemudipersis.annisaservice.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.15
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicResponse {
    private Timestamp timestamp;
    private int status;
    private Object message;

    public static BasicResponse generateResponse(int statusCode, Object msg) {
        return BasicResponse.builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(statusCode)
                .message(msg)
                .build();
    }
}
