package org.pemudipersis.annisaservice.dto.monografi;

import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.pemudipersis.annisaservice.dto.jamiyyah.GeographicDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.JamiyyahPwDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.ProvinsiDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.54
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MonografiPwDto extends GeographicDto {
    @Id
    private Long id;

    private WilayahDto wilayah;

    private String namaPw;

    private ProvinsiDto provinsi;

    private String foto;

    private JamiyyahPwDto jamiyyah;
}
