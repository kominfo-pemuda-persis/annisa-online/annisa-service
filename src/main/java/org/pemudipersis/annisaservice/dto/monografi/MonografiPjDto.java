package org.pemudipersis.annisaservice.dto.monografi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pemudipersis.annisaservice.dto.jamiyyah.DesaDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KabupatenDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KecamatanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.ProvinsiDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;
import org.pemudipersis.annisaservice.request.CabangRequest;
import org.pemudipersis.annisaservice.request.DaerahRequest;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MonografiPjDto {

    private Long id;

    private String kodePj;

    private String namaPj;

    private CabangRequest kdPc;

    private DaerahRequest kdPd;

    private WilayahDto kdPw;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private KecamatanDto kecamatan;

    private DesaDto desa;

    private double latitude;

    private double longitude;

    private String email;

    private String noTelpon;

    private String alamat;

    private String luas;

    private String bwTimur;

    private String bwBarat;

    private String bwSelatan;

    private String bwUtara;

    private String jarakProvinsi;

    private String jarakKabupaten;

    private String photo;

}
