package org.pemudipersis.annisaservice.dto.monografi;

import lombok.Value;
import org.springframework.data.domain.Page;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.53
 */
@Value
public class MonografiPdListDto {
    Page<MonografiPdDto> monografiPd;

//    public static MonografiPdListDto valueOf(Page<MonografiPd> monografiPds) {
//        //Page<MonografiPdDto> pdDtos = monografiPds.map(MonografiPdDto::from);
//        return null;
//    }
}

