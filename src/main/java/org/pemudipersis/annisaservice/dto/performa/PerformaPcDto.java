package org.pemudipersis.annisaservice.dto.performa;

import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.pemudipersis.annisaservice.dto.QuestionDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KabupatenDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KecamatanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.ProvinsiDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;
import org.pemudipersis.annisaservice.request.CabangRequest;
import org.pemudipersis.annisaservice.request.DaerahRequest;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 05.05
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PerformaPcDto extends QuestionDto {
    @Id
    private Long id;

    private CabangRequest cabang;

    private DaerahRequest daerah;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private KecamatanDto kecamatan;

    private String alamat;

    private String ketuaPc;

    private String noHp;
}
