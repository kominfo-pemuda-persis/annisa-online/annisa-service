package org.pemudipersis.annisaservice.dto.performa;

import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.pemudipersis.annisaservice.dto.QuestionDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.ProvinsiDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 05.09
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PerformaPwDto extends QuestionDto {
    @Id
    private Long id;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private String alamat;

    private String ketuaPw;

    private String noHp;
}
