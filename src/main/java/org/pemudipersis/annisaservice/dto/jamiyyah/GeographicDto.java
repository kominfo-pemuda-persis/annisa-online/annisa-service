package org.pemudipersis.annisaservice.dto.jamiyyah;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.46
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class GeographicDto {
    private double latitude;

    private double longitude;

    private String email;

    private String alamatLengkap;

    private String alamatAlternatif;

    private String noKontak;

    private int luas;

    private String btsWilayahUtara;

    private String btsWilayahSelatan;

    private String btsWilayahTimur;

    private String btsWilayahBarat;

    private int jarakIbuKotaNegara;

    private int jarakIbuKotaProvinsi;

    private int jarakIbuKotaKabupaten;

}
