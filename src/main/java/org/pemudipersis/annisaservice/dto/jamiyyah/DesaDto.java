package org.pemudipersis.annisaservice.dto.jamiyyah;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DesaDto {
    @NotEmpty
    private String id;

    //private KecamatanDto kecamatan;

    private String nama;
}
