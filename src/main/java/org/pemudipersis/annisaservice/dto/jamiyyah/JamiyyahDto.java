package org.pemudipersis.annisaservice.dto.jamiyyah;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.46
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class JamiyyahDto {
    private String ketua;

    private String wklKetua;

    private String sekretaris;

    private String wklSekretaris;

    private String bendahara;

    private String wklBendahara;

    private String bidJamiyyah;

    private String wklBidJamiyyah;

    private String bidKaderisasi;

    private String wklBidKaderisasi;

    private String bidAdministrasi;

    private String wklBidAdministrasi;

    private String bidPendidikan;

    private String wklBidPendidikan;

    private String bidDakwah;

    private String wklBidDakwah;

    private String bidHumasPublikasi;

    private String wklBidHumasPublikasi;

    private String bidHal;

    private String wklBidHal;

    private String bidOrgSeni;

    private String wklBidOrgSeni;

    private String bidSosial;

    private String bidEkonomi;

    private String wklBidEkonomi;

    private String penasehat1;

    private String penasehat2;

    private String penasehat3;

    private String pembantuUmum1;

    private String pembantuUmum2;

    private String pembantuUmum3;
}

