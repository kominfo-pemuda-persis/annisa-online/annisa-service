package org.pemudipersis.annisaservice.dto.anggota;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.pemudipersis.annisaservice.dto.jamiyyah.DesaDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KabupatenDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.KecamatanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.MasterPendidikanDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.ProvinsiDto;
import org.pemudipersis.annisaservice.dto.jamiyyah.WilayahDto;
import org.pemudipersis.annisaservice.request.CabangRequest;
import org.pemudipersis.annisaservice.request.DaerahRequest;
import org.pemudipersis.annisaservice.util.validation.ValidateString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class AnggotaRequestDto {

    @NotEmpty(message = "npa is Mandatory")
    private String npa;

    private String nama;

    private String tempat;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalLahir;

    @ValidateString(acceptedValues = {"Single", "Menikah", "Duda"})
    private String statusMerital;

    private String pekerjaan;

    @Valid
    private WilayahDto pw;

    @Valid
    private DaerahRequest pd;

    @Valid
    private CabangRequest pc;

    private String pj;

    private String namaPj;

    private DesaDto desa;

    @Valid
    private ProvinsiDto provinsi;

    @Valid
    private KabupatenDto kabupaten;

    @Valid
    private KecamatanDto kecamatan;

    private String golDarah;

    @Email
    @NotBlank(message = "Email is mandatory")
    private String email;

    private String noTelpon;

    private String alamat;

    @ValidateString(
            acceptedValues = {
                    "Biasa",
                    "Tersiar",
                    "Kehormatan"
            },
            message = "Status Aktif must {acceptedValues}"
    )
    private String jenisKeanggotaan;

    @ValidateString(
            acceptedValues = {
                    "Aktif",
                    "Meninggal",
                    "Non Aktif",
                    "Belum Heregistrasi",
                    "Anggota Baru"
            },
            message = "Status Aktif must {acceptedValues}"
    )
    private String statusAktif;

    private String foto;

    private int idOtonom;

    @Valid
    private MasterPendidikanDto jenjangPendidikan;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate masaAktifKta;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    private String levelTafiq;

}
