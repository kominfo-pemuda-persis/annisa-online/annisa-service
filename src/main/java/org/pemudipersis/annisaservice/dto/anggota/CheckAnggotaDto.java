package org.pemudipersis.annisaservice.dto.anggota;

import lombok.Value;
import org.pemudipersis.annisaservice.entity.anggota.Anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 11.28
 */

@Value
public class CheckAnggotaDto {
    String npa;
    String nama;
    String email;

    private CheckAnggotaDto(Anggota anggota) {
        npa = anggota.getNpa();
        nama = anggota.getNama();
        email = anggota.getEmail();
    }

    public static CheckAnggotaDto valueOf(Anggota anggota) {
        return new CheckAnggotaDto(anggota);
    }
}
