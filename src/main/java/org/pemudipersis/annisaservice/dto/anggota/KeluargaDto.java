package org.pemudipersis.annisaservice.dto.anggota;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pemudipersis.annisaservice.entity.anggota.Hubungan;
import org.pemudipersis.annisaservice.entity.anggota.Status;
import org.pemudipersis.annisaservice.entity.jamiyyah.Otonom;
import org.pemudipersis.annisaservice.util.validation.ValidateString;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KeluargaDto {
    private Long idKeluarga;

    @NotEmpty(message = "nama keluarga not empty")
    private String namaKeluarga;

    @ValidateString(
            acceptedValues = {
                    "Ayah",
                    "Ibu",
                    "Suami",
                    "Anak"
            },
            message = "Hubungan must match : {acceptedValues}"
    )
    private Hubungan hubungan;

    private int jumlahAnak;

    private String alamat;

    private AnggotaDto anggota;

    private Otonom otonom;

    private String keterangan;

    @ValidateString(
            acceptedValues = {
                    "ANGGOTA",
                    "BUKAN ANGGOTA"
            },
            message = "Status Anggota must match : {acceptedValues}"
    )
    private Status statusAnggota;

}

