package org.pemudipersis.annisaservice.dto.anggota;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class BaseImageDto {
    private String name;
    private String imgScr;
}