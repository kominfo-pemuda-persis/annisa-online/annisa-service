package org.pemudipersis.annisaservice.dto.anggota;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pemudipersis.annisaservice.dto.jamiyyah.MasterPendidikanDto;
import org.pemudipersis.annisaservice.entity.anggota.JenisPendidikan;
import org.pemudipersis.annisaservice.util.validation.ValidateString;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 05.00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PendidikanDto {
    private Long idPendidikan;

    private MasterPendidikanDto jenjangPendidikan;

    private String instansi;

    private String jurusan;

    private int tahunMasuk;

    private int tahunKeluar;

    @ValidateString(
            acceptedValues = {
                    "FORMAL",
                    "NON_FORMAL"
            },
            message = "Jenis Pendidikan must match : {acceptedValues}"
    )
    private JenisPendidikan jenisPendidikan;
}
