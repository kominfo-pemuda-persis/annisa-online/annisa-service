package org.pemudipersis.annisaservice.dto.anggota;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrganisasiDto {
    private Long idOrganisasi;

    //private Anggota anggota;

    private String namaOrganisasi;

    private String jabatan;

    private int tahunMulai;

    private int tahunSelesai;

    private String lokasi;
}