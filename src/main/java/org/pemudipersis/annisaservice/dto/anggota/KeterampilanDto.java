package org.pemudipersis.annisaservice.dto.anggota;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KeterampilanDto {

    private Long idKeterampilan;

    //  private Anggota anggota;

    private String keterampilan;
}

