package org.pemudipersis.annisaservice.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailAnggotaPerWilayah {
    private String namaPc;
    private String namaPw;
    private String namaPd;
    private Long jumlah;
}
