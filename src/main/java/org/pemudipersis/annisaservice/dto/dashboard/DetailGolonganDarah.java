package org.pemudipersis.annisaservice.dto.dashboard;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailGolonganDarah {
    @JsonProperty("golonganDarah")
    private String type;

    @JsonProperty("jumlah")
    private Long value;
}
