package org.pemudipersis.annisaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnisaServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnnisaServiceApplication.class, args);
    }

}
