package org.pemudipersis.annisaservice.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.24
 */
@Data
@Builder
public class BaseResponse<T> {

    private HttpStatus status;

    private long took;

    private String message;

    private T data;

}