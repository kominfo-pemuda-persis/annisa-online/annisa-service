package org.pemudipersis.annisaservice.exception;

import jakarta.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.25
 */
@ControllerAdvice
@Log4j2
public class GlobalException extends ResponseEntityExceptionHandler {

    public static final String ROOT_CAUSE = "\n rootCause : ";

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(BusinessException ex) {

        log.error("\n errorCode : " + ex.getErrorCode() + "\n errorMessage :" + ex.getMessage());

        ApiError apiError = new ApiError(
                LocalDateTime.now(),
                ex.getHttpStatus(),
                ex.getHttpStatus().getReasonPhrase().concat(" Data"),
                null,
                Collections.singletonList(ex.getMessage()));

        return ResponseEntityBuilder.build(apiError);

    }


    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        log.error(ROOT_CAUSE, ex);

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        ApiError apiError = new ApiError(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Invalid JSON",
                null,
                errors);

        return ResponseEntityBuilder.build(apiError);
    }

    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        Throwable mostSpecificCause = ex.getMostSpecificCause();

        log.error(ROOT_CAUSE, ex);

        ApiError apiError = new ApiError(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Malformed JSON request",
                null,
                Collections.singletonList(mostSpecificCause.getMessage()));

        return ResponseEntityBuilder.build(apiError);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(Exception ex) {

        log.error(ROOT_CAUSE, ex);

        ApiError err = new ApiError(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Constraint Violation",
                null,
                Collections.singletonList(ex.getMessage()));

        return ResponseEntityBuilder.build(err);
    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {

        log.error(ROOT_CAUSE, ex);

        ApiError apiError = new ApiError(LocalDateTime.now(),
                HttpStatus.FORBIDDEN,
                "Access Denied",
                null,
                Collections.singletonList(ex.getMessage())
        );

        return ResponseEntityBuilder.build(apiError);
    }


    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        log.error(ROOT_CAUSE, ex);

        ApiError apiError = new ApiError(LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Missing Parameters",
                null,
                Collections.singletonList(ex.getParameterName() + " parameter is missing")
        );

        return ResponseEntityBuilder.build(apiError);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception ex) {

        log.error(ROOT_CAUSE, ex);

        ApiError apiError = new ApiError(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Error occurred",
                null,
                Collections.singletonList(ex.getLocalizedMessage()));

        return ResponseEntityBuilder.build(apiError);

    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex) {
        log.error(ROOT_CAUSE, ex);

        ApiError err = new ApiError(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Mismatch Type",
                null,
                Collections.singletonList(ex.getMessage()));

        return ResponseEntityBuilder.build(err);
    }

    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));

        ApiError apiError = new ApiError(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Invalid JSON",
                null,
                Collections.singletonList(builder.toString()));

        return ResponseEntityBuilder.build(apiError);

    }

//    @ExceptionHandler(value = {SdkClientException.class})
//    protected ResponseEntity<Object> handleAmazonException(SdkClientException ex) {
//        log.error(ROOT_CAUSE, ex);
//
//        String errorMessage = ex.getMessage();
//        if(ex instanceof AmazonS3Exception && "NoSuchKey".equals(((AmazonS3Exception) ex).getErrorCode())){
//            errorMessage = "FILE NOT FOUND";
//        }
//        ApiError err = new ApiError(
//                LocalDateTime.now(),
//                HttpStatus.INTERNAL_SERVER_ERROR,
//                "ERROR ",
//                null,
//                Collections.singletonList(errorMessage));
//
//        return ResponseEntityBuilder.build(err);
//    }
}
