package org.pemudipersis.annisaservice.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.26
 */
public class ResponseBuilder<T> {
    public static <T> T buildResponse(HttpStatus status, long took, String message, Object data) {
        return (T) BaseResponse.builder()
                .status(status)
                .data(data)
                .message(message)
                .took(took)
                .build();
    }
}

