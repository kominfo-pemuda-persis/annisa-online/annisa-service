package org.pemudipersis.annisaservice.util;

import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.29
 */
@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StringUtil {

    /**
     * Convert string to compatible {@link InternetAddress}
     * It wraps a mandatory try-catch to avoid incosistency usage of the {@link InternetAddress} constructor
     *
     * @param value is a {@code String} to be converted
     * @return parsed {@code InternetAddress} in {@code toString} form if successfully parsed
     * otherwise return the original {@code String} value
     */
    public static String convertToInternetAddress(@NotNull String value) {
        try {
            return new InternetAddress(value).toString();
        } catch (AddressException e) {
            log.warn("Failed parsing InternetAddress from: {}", value);
            return value;
        }
    }
}
