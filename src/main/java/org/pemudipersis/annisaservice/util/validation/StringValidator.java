package org.pemudipersis.annisaservice.util.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 04.40
 */
public class StringValidator implements ConstraintValidator<ValidateString, String> {
    private Map<String, String> stringValue;

    @Override
    public void initialize(ValidateString annotation) {
        stringValue = new LinkedHashMap<>();
        for (String val : annotation.acceptedValues()) {
            stringValue.put(val, val);
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null || value.length() == 0) {
            return true;
        }
        return Optional.ofNullable(stringValue.get(value)).isPresent();
    }
}
