package org.pemudipersis.annisaservice.entity.monografi;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pemudipersis.annisaservice.entity.jamiyyah.Cabang;
import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.pemudipersis.annisaservice.entity.jamiyyah.Desa;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kecamatan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Provinsi;
import org.pemudipersis.annisaservice.entity.jamiyyah.Wilayah;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/04/21
 * Time: 05.45
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_monografi_pj")
public class MonografiPj implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "kode_pj", nullable = false)
    private String kodePj;

    @Column(name = "nama_pj", nullable = false)
    private String namaPj;

    @OneToOne
    @JoinColumn(
            name = "kd_pc",
            referencedColumnName = "kd_pc",
            foreignKey = @ForeignKey(name = "t_monografi_pj_kd_pc_foreign"),
            nullable = false)
    @JsonDeserialize(as = Cabang.class)
    @JsonSerialize(as = Cabang.class)
    private Cabang kdPc;

    @OneToOne
    @JoinColumn(
            name = "kd_pd",
            referencedColumnName = "kd_pd",
            foreignKey = @ForeignKey(name = "t_monografi_pj_kd_pd_foreign"),
            nullable = false)
    @JsonDeserialize(as = Daerah.class)
    @JsonSerialize(as = Daerah.class)
    private Daerah kdPd;

    @OneToOne
    @JoinColumn(
            name = "kd_pw",
            referencedColumnName = "kd_pw",
            foreignKey = @ForeignKey(name = "t_monografi_pj_kd_pw_foreign"),
            nullable = false)
    @JsonDeserialize(as = Wilayah.class)
    @JsonSerialize(as = Wilayah.class)
    private Wilayah kdPw;

    @OneToOne
    @JoinColumn(
            name = "provinsi",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_monografi_pj_provinsi_foreign"),
            nullable = false)
    @JsonDeserialize(as = Provinsi.class)
    @JsonSerialize(as = Provinsi.class)
    private Provinsi provinsi;

    @OneToOne
    @JoinColumn(
            name = "kabupaten",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_monografi_pj_kabupaten_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kabupaten.class)
    @JsonSerialize(as = Kabupaten.class)
    private Kabupaten kabupaten;

    @OneToOne
    @JoinColumn(
            name = "kecamatan",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_monografi_pj_kecamatan_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kecamatan.class)
    @JsonSerialize(as = Kecamatan.class)
    private Kecamatan kecamatan;

    @OneToOne
    @JoinColumn(
            name = "desa",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_monografi_pj_desa_foreign"),
            nullable = false)
    @JsonDeserialize(as = Desa.class)
    @JsonSerialize(as = Desa.class)
    private Desa desa;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    @Column(name = "longitude", nullable = false)
    private double longitude;

    @Email
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "no_telpon", nullable = false)
    private String noTelpon;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    @Column(name = "luas", nullable = false)
    private String luas;

    @Column(name = "bw_timur", nullable = false)
    private String bwTimur;

    @Column(name = "bw_barat", nullable = false)
    private String bwBarat;

    @Column(name = "bw_selatan", nullable = false)
    private String bwSelatan;

    @Column(name = "bw_utara", nullable = false)
    private String bwUtara;

    @Column(name = "jarak_provinsi", nullable = false)
    private double jarakProvinsi;

    @Column(name = "jarak_kabupaten", nullable = false)
    private double jarakKabupaten;

    @Column(name = "photo")
    private String photo;

    @Column(name = "created_by")
    private String createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "updated_by")
    private String updatedBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "updated_at")
    private Timestamp updatedAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "deleted_at")
    private Timestamp deletedAt;
}

