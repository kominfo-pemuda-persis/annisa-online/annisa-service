package org.pemudipersis.annisaservice.entity.monografi;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.pemudipersis.annisaservice.entity.jamiyyah.Cabang;
import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.pemudipersis.annisaservice.entity.jamiyyah.Geographic;
import org.pemudipersis.annisaservice.entity.jamiyyah.JamiyyahPc;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kecamatan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Provinsi;
import org.pemudipersis.annisaservice.entity.jamiyyah.Wilayah;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/04/21
 * Time: 05.47
 */
@Getter
@Setter
@Entity
@Table(name = "t_monografi_pc")
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class MonografiPc extends Geographic implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pc")
    private Cabang cabang;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pd")
    private Daerah daerah;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pw")
    private Wilayah wilayah;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provinsi")
    private Provinsi provinsi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kota")
    private Kabupaten kabupaten;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kecamatan")
    private Kecamatan kecamatan;

    @OneToOne(mappedBy = "monografiPc", cascade = CascadeType.ALL)
    private JamiyyahPc jamiyyahPc;

    @Column(name = "foto")
    private String foto;

    @Column(name = "created_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;

    @Column(name = "updated_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedDate;
}

