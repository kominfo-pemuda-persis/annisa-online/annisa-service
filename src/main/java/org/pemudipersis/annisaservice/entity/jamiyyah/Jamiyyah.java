package org.pemudipersis.annisaservice.entity.jamiyyah;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 10.33
 */
@Data
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class Jamiyyah {
    @Column(name = "ketua")
    private String ketua;

    @Column(name = "wkl_ketua")
    private String wklKetua;

    @Column(name = "sekretaris")
    private String sekretaris;

    @Column(name = "wkl_sekretaris")
    private String wklSekretaris;

    @Column(name = "bendahara")
    private String bendahara;

    @Column(name = "wkl_bendahara")
    private String wklBendahara;

    @Column(name = "bid_jamiyyah")
    private String bidJamiyyah;

    @Column(name = "wkl_bid_jamiyyah")
    private String wklBidJamiyyah;

    @Column(name = "bid_kaderisasi")
    private String bidKaderisasi;

    @Column(name = "wkl_bid_kaderisasi")
    private String wklBidKaderisasi;

    @Column(name = "bid_administrasi")
    private String bidAdministrasi;

    @Column(name = "wkl_bid_administrasi")
    private String wklBidAdministrasi;

    @Column(name = "bid_pendidikan")
    private String bidPendidikan;

    @Column(name = "wkl_bid_pendidikan")
    private String wklBidPendidikan;

    @Column(name = "bid_dakwah")
    private String bidDakwah;

    @Column(name = "wkl_bid_dakwah")
    private String wklBidDakwah;

    @Column(name = "bid_humas_publikasi")
    private String bidHumasPublikasi;

    @Column(name = "wkl_bid_humas_publikasi")
    private String wklBidHumasPublikasi;

    @Column(name = "bid_hal")
    private String bidHal;

    @Column(name = "wkl_bid_hal")
    private String wklBidHal;

    @Column(name = "bid_or_seni")
    private String bidOrgSeni;

    @Column(name = "wkl_bid_or_seni")
    private String wklBidOrgSeni;

    @Column(name = "bid_sosial")
    private String bidSosial;

    @Column(name = "bid_ekonomi")
    private String bidEkonomi;

    @Column(name = "wkl_bid_ekonomi")
    private String wklBidEkonomi;

    @Column(name = "penasehat1")
    private String penasehat1;

    @Column(name = "penasehat2")
    private String penasehat2;

    @Column(name = "penasehat3")
    private String penasehat3;

    @Column(name = "pembantu_umum1")
    private String pembantuUmum1;

    @Column(name = "pembantu_umum2")
    private String pembantuUmum2;

    @Column(name = "pembantu_umum3")
    private String pembantuUmum3;

    @Column(name = "deleted_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;
}

