package org.pemudipersis.annisaservice.entity.jamiyyah;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 05.39
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_pd")
@Data
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Daerah implements Serializable {
    @Id
    @Column(name = "kd_pd", nullable = false)
    private String kdPd;

    @Column(name = "nama_pd", nullable = false)
    private String namaPd;

    @Column(name = "diresmikan")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate diresmikan;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "created_at")
    private Timestamp createdAt;

    /*@Column(name = "created_by")
    private String createdBy;*/

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "updated_at")
    private Timestamp updatedAt;

//    @Column(name = "updated_by")
//    private String updatedBy;

/*    @Column(name = "is_deleted", columnDefinition = "boolean default false")
    private boolean isDeleted;*/
}

