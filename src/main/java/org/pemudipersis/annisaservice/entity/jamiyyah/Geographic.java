package org.pemudipersis.annisaservice.entity.jamiyyah;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/04/21
 * Time: 05.52
 */
@Data
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class Geographic {
    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "email")
    private String email;

    @Column(name = "alamat_utama")
    private String alamatLengkap;

    @Column(name = "alamat_alternatif")
    private String alamatAlternatif;

    @Column(name = "no_kontak")
    private String noKontak;

    @Column(name = "luas")
    private int luas;

    @Column(name = "bw_utara")
    private String btsWilayahUtara;

    @Column(name = "bw_selatan")
    private String btsWilayahSelatan;

    @Column(name = "bw_timur")
    private String btsWilayahTimur;

    @Column(name = "bw_barat")
    private String btsWilayahBarat;

    @Column(name = "jarak_dari_ibukota_negara")
    private double jarakIbuKotaNegara;

    @Column(name = "jarak_dari_ibukota_provinsi")
    private double jarakIbuKotaProvinsi;

    @Column(name = "jarak_dari_ibukota_kabupaten")
    private double jarakIbuKotaKabupaten;

    @Column(name = "deleted_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;
}
