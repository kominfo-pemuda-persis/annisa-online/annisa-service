package org.pemudipersis.annisaservice.entity.performa;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.pemudipersis.annisaservice.entity.jamiyyah.Cabang;
import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.pemudipersis.annisaservice.entity.jamiyyah.Desa;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kecamatan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Provinsi;
import org.pemudipersis.annisaservice.entity.jamiyyah.Wilayah;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 05.46
 */
@Getter
@Setter
@Entity
@Table(name = "t_performa_pj")
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class PerformaPj extends Question {
    @Id
    @Column(name = "kd")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "kd_pj")
    private String kodePj;

    private String namaPj;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pc")
    private Cabang cabang;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pd")
    private Daerah daerah;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pw")
    private Wilayah wilayah;

    private String alamat;

    private String ketuaPj;

    private String noHp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provinsi")
    private Provinsi provinsi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kota")
    private Kabupaten kabupaten;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kecamatan")
    private Kecamatan kecamatan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desa")
    private Desa desa;
}
