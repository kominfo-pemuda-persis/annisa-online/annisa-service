package org.pemudipersis.annisaservice.entity.anggota;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.46
 */
@Entity
@Table(name = "t_anggota_pendidikan")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pendidikan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPendidikan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_anggota")
    private Anggota anggota;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_master_pendidikan")
    private MasterPendidikan jenjangPendidikan;

    private String instansi;

    private String jurusan;

    private int tahunMasuk;

    private int tahunKeluar;

    @Column(name = "jenisPendidikan", columnDefinition = "ENUM('FORMAL', 'NON FORMAL')")
    @Enumerated(EnumType.STRING)
    private JenisPendidikan jenisPendidikan;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;

}
