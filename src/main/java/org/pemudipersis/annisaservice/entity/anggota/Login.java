package org.pemudipersis.annisaservice.entity.anggota;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pemudipersis.annisaservice.entity.user.Permission;
import org.pemudipersis.annisaservice.entity.user.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/04/21
 * Time: 12.02
 */
@Entity
@Table(name = "t_login")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Login implements UserDetails {
    @Id
    @Column(name = "id_login", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String idLogin;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "npa",
            referencedColumnName = "npa",
            foreignKey = @ForeignKey(name = "t_login_npa_foreign"),
            nullable = false)
    private Anggota anggota;

    @Column(name = "username", nullable = false, unique = true)
    @Size(max = 50)
    private String username;

    @Email
    @Column(name = "email", nullable = false)
    @Size(max = 50)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "activ_code")
    private String activCode;
    @Column(name = "email_confirm", nullable = false)
    private Integer emailConfirm;
    @Column(name = "remember_token")
    private String rememberToken;
    @Column(name = "status_aktif", nullable = false)
    private Integer statusAktif;
    @Column(name = "created_at")
    private Timestamp createdAt;
    @Column(name = "updated_at")
    private Timestamp updatedAt;

    @ManyToMany
    @JoinTable(name = "t_role_user",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id_login"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id_role"))
    private Set<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<Permission> permissions = new HashSet<>();
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (roles != null) {
            roles.forEach(role -> {
                if (role.getPermissions() != null) {
                    permissions.addAll(role.getPermissions());
                }
            });
            permissions.forEach(permission -> {
                if (permission != null) {
                    authorities.add(new SimpleGrantedAuthority(permission.getModule().concat("_").concat(permission.getPermission())));
                }
            });

        }
        return authorities;

    }

    @Override
    public String getUsername() {
        return anggota.getNpa();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return statusAktif == 1;
    }
}
