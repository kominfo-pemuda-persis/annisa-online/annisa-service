package org.pemudipersis.annisaservice.entity.anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 09.51
 */
public enum JenisTraining {
    TRAINING_PEMUDA,
    TRAINING_LUAR_PEMUDA
}
