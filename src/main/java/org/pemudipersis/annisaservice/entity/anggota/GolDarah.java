package org.pemudipersis.annisaservice.entity.anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/04/21
 * Time: 07.12
 */
public enum GolDarah {
    A, B, AB, O
}
