package org.pemudipersis.annisaservice.entity.anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 08.31
 */
public enum Hubungan {
    AYAH,
    IBU,
    SUAMI,
    ANAK
}
