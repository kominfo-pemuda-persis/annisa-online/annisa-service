package org.pemudipersis.annisaservice.entity.anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 06.09
 */
public enum StatusAktif {
    ACTIVE("001"),
    NON_ACTIVE("002"),
    TIDAK_HEREGISTRASI("003"),
    MUTASI_KE_PERSISTRI("004"),
    MENINGGAL("005"),
    MENGUNDURKAN_DIRI("005");

    private final String statusAktif;

    StatusAktif(String statusAktif) {

        this.statusAktif = statusAktif;
    }

    public String getStatusAktif() {
        return this.statusAktif;
    }
}
