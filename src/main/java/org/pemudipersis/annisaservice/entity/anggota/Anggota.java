package org.pemudipersis.annisaservice.entity.anggota;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.pemudipersis.annisaservice.entity.jamiyyah.Cabang;
import org.pemudipersis.annisaservice.entity.jamiyyah.Daerah;
import org.pemudipersis.annisaservice.entity.jamiyyah.Desa;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kabupaten;
import org.pemudipersis.annisaservice.entity.jamiyyah.Kecamatan;
import org.pemudipersis.annisaservice.entity.jamiyyah.Provinsi;
import org.pemudipersis.annisaservice.entity.jamiyyah.Wilayah;
import org.pemudipersis.annisaservice.service.anggota.JenisKeanggotaan;
import org.pemudipersis.annisaservice.service.anggota.StatusMerital;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.24
 */
@Entity
@Table(name = "t_anggota")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = {"pw", "pd", "pc", "desa", "provinsi", "kabupaten", "kecamatan", "jenjangPendidikan"})
public class Anggota implements Serializable {

    private static final long serialVersionUID = -2155753233215409928L;

    @Id
    @Column(name = "id_anggota", nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Size(max = 32)
    private String id;

    @Column(name = "npa")
    private String npa;

    @Column(name = "nama_lengkap")
    private String nama;

    @Column(name = "tempat_lahir")
    private String tempat;

    @Column(name = "tanggal_lahir")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalLahir;

    @Column(name = "status_merital", columnDefinition = "ENUM('SINGLE', 'MENIKAH', 'JANDA')")
    @Enumerated(EnumType.STRING)
    private StatusMerital statusMerital;

    @Column(name = "pekerjaan")
    private String pekerjaan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pw")
    private Wilayah pw;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pd")
    private Daerah pd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pc")
    private Cabang pc;

    @Column(name = "pj")
    private String pj;

    private String namaPj;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desa")
    private Desa desa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provinsi")
    private Provinsi provinsi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kota")
    private Kabupaten kabupaten;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kecamatan")
    private Kecamatan kecamatan;

    @Column(name = "gol_darah", columnDefinition = "ENUM('A', 'B', 'AB', 'O')")
    @Enumerated(EnumType.STRING)
    private GolDarah golDarah;

    @Email
    @NotBlank(message = "Email is mandatory")
    @Column(name = "email")
    private String email;

    @Column(name = "no_telpon")
    private String noTelpon;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "jenis_keanggotaan", columnDefinition = "ENUM('TERSIAR', 'BIASA')")
    @Enumerated(EnumType.STRING)
    private JenisKeanggotaan jenisKeanggotaan;

    @Column(name = "status_aktif", columnDefinition = "ENUM('ACTIVE', 'NON-ACTIVE', 'TIDAK HEREGISTRASI', 'MUTASI KE " +
            "PERSISTRI', 'MENINGGAL', 'MENGUNDURKAN DIRI')")
    @Enumerated(EnumType.STRING)
    private StatusAktif statusAktif;

    @Column(name = "nik")
    private String nik;

    @Column(name = "kk")
    private String kk;

    @Column(name = "ktp_image")
    private String ktpImage;

    @Column(name = "foto")
    private String foto;

    @Column(name = "id_otonom")
    private short idOtonom;

    @Column(name = "masa_aktif_kta")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate masaAktifKta;

    @Column(name = "reg_date")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    @Column(name = "last_updated")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime lastUpdated;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;

    @Column(name = "deleted_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;

    @ManyToOne
    @JoinColumn(name = "pendidikan_terakhir", nullable = false)
    private MasterPendidikan jenjangPendidikan;

    private short levelPkp;
}
