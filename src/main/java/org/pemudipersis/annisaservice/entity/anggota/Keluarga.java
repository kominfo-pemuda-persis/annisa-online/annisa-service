package org.pemudipersis.annisaservice.entity.anggota;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pemudipersis.annisaservice.entity.jamiyyah.Otonom;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/04/21
 * Time: 21.39
 */
@Entity
@Table(name = "t_anggota_keluarga")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Keluarga implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idKeluarga;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_anggota")
    private Anggota anggota;

    private String namaKeluarga;

    @Column(name = "hubungan", columnDefinition = "ENUM('AYAH', 'IBU', 'SUAMI', 'ANAK')")
    @Enumerated(EnumType.STRING)
    private Hubungan hubungan;

    private int jumlahAnak;

    private String alamat;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_otonom")
    private Otonom otonom;

    private String keterangan;

    @Column(name = "status_anggota", columnDefinition = "ENUM('ANGGOTA','NON ANGGOTA')")
    @Enumerated(EnumType.STRING)
    private Status statusAnggota;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;

}
