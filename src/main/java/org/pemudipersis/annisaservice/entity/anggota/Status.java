package org.pemudipersis.annisaservice.entity.anggota;

/**
 * Created by IntelliJ IDEA.
 * Project : Annisa Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/21
 * Time: 08.40
 */
public enum Status {
    ANGGOTA("ANGGOTA"),
    NON_ANGGOTA("NON ANGGOTA");

    private final String statusAnggota;

    Status(String statusAnggota) {

        this.statusAnggota = statusAnggota;
    }

    public String getStatusAnggota() {
        return this.statusAnggota;
    }
}
