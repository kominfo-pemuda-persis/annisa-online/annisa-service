# Annisa Service
Annisa Service merupakan API Service yang mengelola administrasi Anggota Pemudi Persis di belakang layar. Dibuat dengan penuh cinta menggunakan Spring Boot.
## Required tools should be installed before You run the application
1. [JDK11](https://adoptopenjdk.net/)
2. [Maven](https://maven.apache.org/)
3. [Mysql](https://www.mysql.com/)

## Things to do list:
1. Run Mysql
2. Clone this project: `git clone https://gitlab.com/kominfo-pemuda-persis/annisa-online/annisa-service.git`
3. Go inside the folder: `cd annisa-service`
4. Run the application: `mvn clean spring-boot:run`

## To see the available REST API endpoint:
Open your browser then go to [Swagger UI](http://localhost:8080/swagger-ui/index.html)

## Test REST API using Postman: